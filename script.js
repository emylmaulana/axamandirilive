
$(document).ready(function() {

$('#topbarselect, .topbarselect2').on('change', function (e) {
  var valueSelected = this.value;
  window.open(valueSelected, '_parent');
});

/* POPUP
==============================*/
$("#bimi").fancybox({
  maxWidth    : 585,
  maxHeight   : 405,
  fitToView   : true,
  width       : 580,
  height      : 400,
  autoSize    : false,
  closeClick  : false,
  openEffect  : 'none',
  closeEffect : 'none',
  padding     : 0,
  scrolling   : "no"
});

$(".neutron").fancybox({
  maxWidth    : 585,
  maxHeight   : 405,
  fitToView   : true,
  width       : 580,
  height      : 400,
  autoSize    : false,
  closeClick  : false,
  openEffect  : 'none',
  closeEffect : 'none',
  padding     : 0,
  scrolling   : "no",
  afterLoad : function(current) {
    jQuery('.fancybox-iframe').contents().find("#topbar").remove();
    jQuery('.fancybox-iframe').contents().find("#header").remove();
    jQuery('.fancybox-iframe').contents().find("#responsiveHead").remove();
    jQuery('.fancybox-iframe').contents().find("#footer").remove();
    jQuery('.fancybox-iframe').contents().find("#website-selector480").remove();
    jQuery('.fancybox-iframe').contents().find("#footer-mobile").remove();
  }
});



/* Uniform Js
==============================*/
  $('select').uniform();

/* Dotdotdot
==============================*/
  $(".details.seven p").dotdotdot({
          watch: 'window'
    });
  
/* Tabs
==============================*/
  var $doc = $(document),
  Modernizr = window.Modernizr;
  $.fn.foundationAccordion        ? $doc.foundationAccordion() : null;
  //$.fn.foundationTabs             ? $doc.foundationTabs() : null;
  $.fn.foundationTabs 			  ? $doc.foundationTabs({deep_linking: false}) : null;
                              
/* Menu Mobile
==============================*/
   $('#menu-mobile').prepend('<div id="menu-icon"> <i class="icon-list"></i></div>');
      $("#menu-icon").on("click", function(){
      $(".list-mobile").slideToggle();
      $(this).toggleClass("active");
   });

/* Mega Menu
==============================*/
    var $menu = $(".dropdown-menu");
    $menu.menuAim({
        activate: activateSubmenu,
        deactivate: deactivateSubmenu
    });

    function activateSubmenu(row) {
      var $row = $(row),
          submenuId = $row.data("submenuId"),
          $submenu = $("#" + submenuId),
          height = $menu.outerHeight(),
          width = $menu.outerWidth();

      // Show the submenu
      $submenu.css({
          display: "block",
          top: 0,
          left: width - 3,  // main should overlay submenu
          height: height - 4  // padding for main dropdown's arrow
      });

      // Keep the currently activated row's highlighted look
      $row.find("a").addClass("maintainHover");
    }

    function deactivateSubmenu(row) {
      var $row = $(row),
          submenuId = $row.data("submenuId"),
          $submenu = $("#" + submenuId);

      // Hide the submenu and remove the row's highlighted look
      $submenu.css("display", "none");
      $row.find("a").removeClass("maintainHover");
    }

    $(".dropdown-menu li").click(function(e) {
        e.stopPropagation();
    });


/* Talk To Us
==============================*/

var top = $('#talktous').offset().top - parseFloat($('#talktous').css('marginTop').replace(/auto/, 0));
  $(window).scroll(function (event) {
   // what the y position of the scroll is
  var y = $(this).scrollTop();

   // whether that's below the form
  if (y >= top) {
  // if so, ad the fixed class
  $('#talktous').addClass('fixed');
  } else {
          // otherwise remove it
  $('#talktous').removeClass('fixed');
    }
  });


/* Swiper
==============================*/
  var mySwiper = new Swiper('.swiper-container',{
    loop: true,
    slidesPerView: 'auto'
    });

  var mySwiper = new Swiper('.properties.tabs.oce',{
    loop: true,
    slidesPerView: 'auto'
    });
  var index = $(".swiper-slide .active").index();
  var position = index ? index : 0;
  var jenggotz = new Swiper('.timeline-slide.tabs',{
      slidesPerView: 'auto',
      freeMode:true,
      paginationClickable: true,
      initialSlide: position
    });
  
  var mySwiper = new Swiper('.float-swiper',{
    loop: true,
    slidesPerView: 'auto'
    });

var featuremini = new Swiper('.featuremini-container',{
    slidesPerView: 'auto',
    loop:false
 
    });
var explore = new Swiper('.explore-container',{
    slidesPerView: 'auto',
    loop:true
 
    });
var cluster = new Swiper('.cluster-container',{
    slidesPerView: 'auto',
    loop:true
 
    });
$('.timeline-slide.tabs .prev').on('click', function(e){
    e.preventDefault()
    jenggotz.swipePrev()
    })
  $('.timeline-slide.tabs .next').on('click', function(e){
    e.preventDefault()
    jenggotz.swipeNext()
    })
  $('#featuremini-slider .prev').on('click', function(e){
    e.preventDefault()
    featuremini.swipePrev()
    })
  $('#featuremini-slider .next').on('click', function(e){
    e.preventDefault()
    featuremini.swipeNext()
    })

  $('.prevmini').on('click', function(e){
    e.preventDefault()
    explore.swipePrev()
    })
  $('.nextmini').on('click', function(e){
    e.preventDefault()
    explore.swipeNext()
    })
    
  // $('#sliderContainer').cycle();

  $('#accordion-2').dcAccordion({
    eventType: 'click',
    autoClose: true,
    saveState: true,
    disableLink: true,
    autoExpand : true,
    classArrow   : 'dcjq-icon',
    speed: 'fast',
    classActive: 'active',
    disableLink : false,
    classParent  : 'parent'
  });

  $('.reports-container').each(function(){
    var swipe = $(this).swiper({
      slidesPerView: 'auto',
      freeMode:true,
      grabCursor: true
    })

    $(".reports-container .prev").on("click", function(e){
      e.preventDefault()
      swipe.swipePrev()
  })

    $(".reports-container .next").on("click", function(e){
      e.preventDefault()
      swipe.swipeNext()
  })
  });


});//end ready

/* Jquery Cycle
==============================*/
jQuery(document).ready(function($){
var slideshows = $('.cycle-slideshow').on('cycle-next cycle-prev', function(e, opts) {
    // advance the other slideshow
    slideshows.not(this).cycle('goto', opts.currSlide);
});

$('#cycle-2 .cycle-slide').click(function(){
    var index = $('#cycle-2').data('cycle.API').getSlideIndex(this);
    slideshows.cycle('goto', index);
});

$("article p:has(img)").css("margin-bottom","0");

});

$("[data-submenu-id*='submenu-financial-news']").remove();


       
$(".dcjq-icon").click(function(){
   $(this).prev().prev().toggleClass("active");
});
   
