<html>
    <head>
        <title><?=$title?></title>
        <style>
            .upload{
                background: none repeat scroll 0 0 #F5F5F5;
                border-bottom: 1px solid #E5E5E5;
                padding:5px;
                margin:5px;
            }
            div img{
                border:5px solid #E5E5E5;
            }
        </style>
    </head>
    <body>
        <?=$body?>
    </body>
</html>