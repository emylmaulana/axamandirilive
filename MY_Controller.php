<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public $local;
    public $data;
    public $lang;

	function __construct() {
        parent::__construct();
        
		if(isset($_GET['lang'])){
            $this->local = '_'.$_GET['lang'];

            $this->data['lang'] = $_GET['lang'];
            $this->session->set_userdata('lang', $this->data['lang']);

            if($_GET['lang'] == 'id')
                set_translation_language($_GET['lang'].'_ID'); 
            else
                set_translation_language($_GET['lang'].'_UK');
        }
        elseif($this->session->userdata('lang') != ''){
            $this->local = '_'.$this->session->userdata('lang');
            $this->data['lang'] = $this->session->userdata('lang');
        }
        else{
            $this->local = '_en';
            $this->data['lang'] = 'en';
        }

        $this->data['megamenu_city'] = $this->model_global->megamenu_city($this->local);
        $this->data['megamenu_discover'] = $this->model_global->megamenu_discover($this->local);
        $this->data['megamenu_facilities'] = $this->model_global->megamenu_facilities($this->local);
        $this->data['megamenu_investor'] = $this->model_global->megamenu_investor($this->local);

        $this->load->library('antispam');
    }
}