<?php/** * Template Name: Contact Us */?>
<?php get_header();?>		
<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=570183&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>		
<div id="page-container" style="background-image:url(<?php the_field('header_image');?>);">
	<div id="masthead" class="row relative">
		<div class="mobile-content absolute" id="header-image" style="background-image:url(<?php the_field('header_image');?>);"></div>
		<div class="content large-4">
			<h1><?php the_title();?></h1>
			<h2 style="color:<?php the_field('subtitle_text_color');?>"><?php the_field('sub_title');?></h2>
		</div><!--end large 4-->

		<div class="show-for-large-only"><?php get_template_part("widget/customer-care");?></div>
	</div><!--end masthead-->

	<div id="wrapper" class="row">  
		<section id="page-half" class="white clearfix sections radius-all-5">
			<div class="large-8 columns">
					 <?php if( have_posts() ) : the_post(); ?>
						<h3 class="m-bottom-30 f-24"><?php the_title();?></h3>
						<!-- form -->
						<form action="javascript:void(0)" id="form" name="form" method="post" class="wpcf7-form" >
							<div style="display: none;">
							<input type="hidden" name="_wpcf7" value="13">
							<input type="hidden" name="_wpcf7_version" value="3.6">
							<input type="hidden" name="_wpcf7_locale" value="id_ID">
							<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f13-p1315-o1">
							<input type="hidden" name="_wpnonce" value="21068c9497">
							<input type="hidden" name="banner_source" value="">
							<input type="hidden" name="utm_source" value="">
							<input type="hidden" name="utm_medium" value="">
							<input type="hidden" name="utm_term" value="">
							<input type="hidden" name="utm_content" value="">
							<input type="hidden" name="utm_campaign" value="">
							<input type="hidden" name="gclid" value="">
							</div>
							<div id="contact-form">
							<div class="fieldset">
							    <label>Nama*</label><br>
							     <span class="wpcf7-form-control-wrap nama" style="width: 508px;"><input type="text" name="nama" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>
							  </div>
							<div class="fieldset">
							    <label>Tanggal Lahir*</label><br>
							     <span class="wpcf7-form-control-wrap tgl_lahir" style="width: 508px;"><input type="text" name="tgl_lahir" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required datepicker " aria-required="true" aria-invalid="false" id="tgl_lahir"></span>
							  </div>
							<div class="fieldset">
							    <label>Alamat*</label><br>
							     <span class="wpcf7-form-control-wrap alamat" style="width: 508px;"><input type="text" name="alamat" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>
							  </div>
							<div class="fieldset">
							    <label>Email*</label><br>
							     <span class="wpcf7-form-control-wrap email" style="width: 508px;"><input type="email" name="email" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span>
							  </div>
							<div class="fieldset">
							    <label>No. HP*</label><br>
							     <span class="wpcf7-form-control-wrap no_hp" style="width: 508px;"><input type="text" name="no_hp" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>
							  </div>
							<div class="fieldset">
							    <label>No. Telp Kantor</label><br>
							     <span class="wpcf7-form-control-wrap no_tlp" style="width: 508px;"><input type="text" name="no_tlp_kantor" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span>
							  </div>
							<div class="fieldset">
							    <label>No. Telp Rumah</label><br>
							     <span class="wpcf7-form-control-wrap no_tlp" style="width: 508px;"><input type="text" name="no_tlp" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span>
							  </div>
							<div class="fieldset">
							    <label>Fax</label><br>
							     <span class="wpcf7-form-control-wrap fax" style="width: 508px;"><input type="text" name="fax" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span>
							  </div>
							<div class="fieldset">
							    <label >Tipe Asuransi</label><br>
							    <span class="wpcf7-form-control-wrap kategori" style="width: 508px;">
							    	
							    	<select id="pilih-entity" name="entity" class="wpcf7-form-control wpcf7-select" aria-invalid="false">
							    		<option value="">---</option>
							    		<?php 
								    		$terms = get_terms("direktori_entity");
											    foreach ( $terms as $term ) { 
											    	$slug = $term->slug; ?>
											   <option value="<?php echo $slug; ?>"><?php echo $term->description; ?></option>											        
											    <?php }
								    	?>
							    	</select>
							    </span>
							  </div>
							<div class="fieldset">
							    <label>Nama Produk</label><br>
							    <span class="wpcf7-form-control-wrap name_product" style="width: 508px;">
									<div id="pilihform-wrap" class="disable relative">
										<select id="pilih-form" name="produk" class="required">
											<option disabled selected><?php _e("<!--:en-->Select Product<!--:--><!--:id-->Pilih Produk<!--:-->"); ?></option>
										</select>
									</div>
								 </span>
							  </div>
							 
							  <script type="text/javascript">
								jQuery(document).ready(function() {	
									$('#pilih-entity').on('change', function (e) {
										var valueSelected = this.value;
										var title = $("#pilih-entity option[value='"+valueSelected+"']").text();
										$('#pilih-form option:selected').text("Loading..").attr('disabled','disabled');
										$('#pilih-form').load('<?php bloginfo('template_url');?>/getNamaProduk.php?place='+valueSelected);
										$('#pilihform-wrap').removeClass('disable');
									});
									$('#pilih-form').on('change', function (e) {
										var valueSelected = this.value;
										var title = $("#pilih-form option[value='"+valueSelected+"']").text();
										$("#a").val(title);
										$("#b").val(valueSelected);
									});				
								});
							</script>

							<div class="fieldset">
							    <label>No. Polis (jika ada)</label><br>
							     <span class="wpcf7-form-control-wrap no_polis" style="width: 508px;"><input type="text" name="no_polis" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span>
							  </div>
							<div class="fieldset">
							    <label>Kategori</label><br>
							    <span class="wpcf7-form-control-wrap kategori" style="width: 508px;">
							    	<select name="kategori" id="kategori" class="wpcf7-form-control wpcf7-select" aria-invalid="false">
							    		<option value="">---</option>
							    		<option value="Pertanyaan Umum">Pertanyaan Umum</option>
							    		<option value="Informasi Produk">Informasi Produk</option>
							    		<option value="Informasi Financial Advisor">Informasi Financial Advisor</option>
							    		<option value="Saran">Saran</option>
							    		<option value="Keluhan">Keluhan</option>
							    		<option value="Pengkinian">Pengkinian Data</option>
							    		<option value="Lainnya">Lainnya</option>
							    	</select>
							    </span>
							  </div>
							  <div class="pengkinian">
							  </div>
							  <div class="hubungikami">
							  	
							</div>
							<div class="fieldset">
								    <label></label><br>
								    <input type="submit" value="Kirim" class="wpcf7-form-control wpcf7-submit button blue m-top-30">
							</div>
							<div class="wpcf7-response-output wpcf7-display-none" style="display: none;"></div>
						<p class="cap_status f-14">*)Harus di isi</p>
						<p class="cap_sukses f-14 " style="display:none"></p>
						</form>
						<!-- form -->
						
						
					<?php endif;?>
			</div><!--end large 8-->
		</div>
			<aside class="columns w-322 m-top-45">
			<div class="widget">
				<strong class="c-blue" style="font-size:1.125em;display:block;">AXA Mandiri</strong><br>
				<ul class="office-details">
	           		<li class="address m-bottom-10"><?php the_field('office_address', 'option');?></li>
	           		<li class="phone"><?php the_field('office_phone', 'option');?></li>
	           		<li class="fax"><?php the_field('office_fax', 'option');?></li>
	           		<li class="email"><a href="mailto:<?php the_field('office_email', 'option');?>"><?php the_field('office_email', 'option');?></a></li>
           		</ul>
			 </div>
			 <div class="widget"><?php get_template_part("widget/sidebar-socmed");?></div>
			 <div class="widget"><?php get_template_part("widget/footer-banner-left");?></div>
			 <div class="widget"><?php get_template_part("widget/footer-banner-right");?></div>
			</aside>
		</section>
		<?php get_template_part("widget/breadcrumbs");?>
	</div><!--end row-->
<?php get_template_part("widget/hargaunit");?>
</div><!--end page container-->
<script type="text/javascript">
			$(document).ready(function(){
				$( ".hubungikami" ).append('<div class="fieldset subject"><label>Subjek*</label><br><span class="wpcf7-form-control-wrap subjek" style="width: 508px;"><input type="text" name="subjek" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span></div><div class="fieldset pesan"><label>Pesan Anda*</label><br><span class="wpcf7-form-control-wrap pesan" style="width: 508px;"><textarea name="pesan" cols="40" rows="10" class="required wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></textarea></span></div>');
				$('#kategori ').change(function() {
					console.log($(this).val());
						if ($(this).val()=='Pengkinian') {
							$('.hubungikami').children().remove();
							$('.pengkinian').children().remove();
							// $('.pengkinian').children().remove();
							$( ".pengkinian" ).append( '<div class="fieldset nama-tertanggung"><label>Nama Tertanggung*</label><br><span class="wpcf7-form-control-wrap subjek" style="width: 508px;"><input type="text" name="nama_tertanggung" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span></div><div class="fieldset cc-bantu"><label style="width:100%;float:none;">Apakah Anda telah dibantu oleh rekan customer care center dalam memperbaharui data ini?</label><br><div class="clerfix m-bottom-20"><input type="radio" name="bantuan" value="1" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">Ya&nbsp;&nbsp;<input type="radio" name="bantuan" value="0" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" checked>Tidak</div><!-- </span> --><!-- <input style="display:none;" id="rekan" type="text" name="cc_bantu" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Nama Rekan yang membantu anda"> --><div id="rekan" class="m-bottom-10" style="display:none;" ><p>Saya menyatakan bahwa informasi yang saya berikan ini adalah data pribadi atau data lainnya adalah benar dan bersama ini saya memberikan persetujuan kepada PT. AXA Mandiri Financial  Services untuk :</p><ul class="f-14"><li class="m-bottom-10">Mengungkapkan data pribadi saya (termasuk data terbaru) kepada pihak ketiga termausk penyedia jasa untuk mengiinkan PT. AXA Mandiri Financial Services dalam memberikan layanan terkait atas produk yang saya beli dan untuk tujuan komersial lainnya.</li><li>Menggunakan data pribadi saya hanya untuk tujuan program pemasaran langsung, penawaran produk, aktifitas lainnya. Oleh karena itu, saya mengizinkan PT. AXA Mandiri Financial Services untuk menghubungi saya dalam rangka memberikan informasi mengenai produk dan layanan  melalui sarana telekomunikasi privat termasuk namun tidak terbatas pada email, telepon, SMS yang telah saya berikan ini. </li></ul><input type="checkbox" name="agreement-spaj" value="1"><span class="inline-block w-80p f-14 m-left-10" style="display:inline-block" required>Mohon beri tanda ✓ pada kotak di samping apabila Anda setuju atau tidak setuju dengan penggunaan data pribadi Anda untuk tujuan diatas</span></div></div>' );
						}else{
							$('.hubungikami').children().remove();
							$('.pengkinian').children().remove();
							$( ".hubungikami" ).append('<div class="fieldset subject"><label>Subjek*</label><br><span class="wpcf7-form-control-wrap subjek" style="width: 508px;"><input type="text" name="subjek" value="" size="40" class="required wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span></div><div class="fieldset pesan"><label>Pesan Anda*</label><br><span class="wpcf7-form-control-wrap pesan" style="width: 508px;"><textarea name="pesan" cols="40" rows="10" class="required wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></textarea></span></div>');
						};
					
				});
				// $('body').onchange(function() {
				// 	if ($(this).val()=='1') {
				// 		$('#rekan').show();
				// 	}else{
				// 		$('#rekan').hide();
				// 	};
				// });
				$('body').on('change', '.cc-bantu input[type=radio]', function() {
					if ($(this).val()=='1') {
						$('#rekan').show();
					}else{
						$('#rekan').hide();
					};
				});
		 		//captcha check
				$("#form").submit(function(){
									
				    if($("#form").valid()) {
						$.ajax({
							type: "POST",
							url: "<?=site_url()?>/axamandiri_form/hubungi_kami/submit_data",
							data: $('form').serialize(),
							success: function(){
								$('.cap_sukses').html("Terima kasih, pesan terkirim.").fadeIn(200).show();
								$('.cap_status').fadeOut(200).hide();
								document.forms["form"].reset();
							}
							});

				    }else{
						$('.cap_sukses').hide();
				    	$('.cap_status').html("Mohon periksa kembali field yang wajib diisi!").addClass('cap_status_error').fadeIn('slow');
				    	
				    }

				    return false;
				 });
				
		 	});
		</script>
		<style type="text/css">
			div.radio, div.radio input, div.radio span{
				float: none;
				display: inline-block;
			}

			.checker{
				display: inline-block;
				vertical-align: top;
			}
		</style>

<?php get_footer();?>
