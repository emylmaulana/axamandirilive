<?php/** * Template Name: Tentang AXA Mandiri */?>
<?php get_header();?>
<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=570167&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
<div id="page-container" style="background-image:url(<?php the_field('header_image');?>);">
	<div id="masthead" class="row relative">
		<div class="content large-4">
			<h1><?php the_title(); ?></h1>
			<h2 style="color:<?php the_field('subtitle_text_color');?>"><?php the_field('sub_title');?></h2>
		</div><!--end large 4-->

		<div class="show-for-large-only"><?php get_template_part("widget/customer-care");?></div>
	</div><!--end masthead-->

	<div id="wrapper" class="row">
		<section id="content-details" class="clearfix">
			<?php if( have_posts() ) : the_post(); ?>
				<?php the_content();?>
			<?php endif;?>

			<ul id="icons" class="clearfix">
				<li class="karyawan"></li>
				<li class="pemasaran"></li>
				<li class="nasabah"></li>
			</ul>
		</section>

		<section id="nilai-axa" class="blue-sections clearfix" style="background-image:url(<?php the_field('blue_sections_images');?>);">
			<span class="blue-sections-cover-left"></span>
			<!-- <span class="blue-sections-cover"></span> -->
			<?php if(get_field('nilai_axa')): ?>
				<?php while(has_sub_field('nilai_axa')): ?>
					<h3><?php _e("<!--:en-->POINTS OF AXA Mandiri<!--:--><!--:id-->NILAI-NILAI AXA Mandiri<!--:-->"); ?></h3>
					<div class="large-6 column first-child">
						<h2 class="f-24 c-white"><?php the_sub_field('title');?></h2>
						<?php the_sub_field('content');?>
					</div>
					<!-- <ul id="icons" class="relative large-6 column">
						<li class="profesionalism">
							<span class="tooltip absolute">
							<strong>Professionalism</strong>
								Kami berkomitmen untuk memberikan pelayanan yang optimal dan berstandar tinggi.
							</span>
						</li>

						<li class="integrity">
							<span class="tooltip absolute">
							<strong>Integrity</strong>
								Kami mengambil langkah nyata untuk memenuhi kebutuhan nasabah dan karyawan secara efisien, akurat, dan terpercaya.
							</span>
						</li>

						<li class="pragmatism">
							<span class="tooltip absolute profesionalism">
							<strong>Pragmatism</strong>
								Kami mengimplementasikan ide ke dalam langkah nyata,<br/>serta mengomunikasikannya secara jelas dan terbuka.
							</span>
						</li>

						<li class="innovation">
							<span class="tooltip absolute profesionalism">
							<strong>Innovation</strong>
								Kami juga secara konsisten menciptakan inovasi baru sebagai nilai tambah bagi semua pemangku kepentingan.
							</span>
						</li>

						<li class="spirit">
							<span class="tooltip absolute profesionalism">
							<strong>Spirit</strong>
								Kami lakukan dengan menjunjung team spirit, semangat kebersamaan sebagai satu perusahaan, AXA.
							</span>
						</li>
					</ul> -->
				<?php endwhile;?>
			<?php endif;?>
			<span class="blue-sections-cover-right"></span>
		</section>

		<section id="axa-group" class="clearfix background-vector" style="display:none;">
			
			<div id="logo3"></div>
			<p><?php _e("<!--:en-->AXA Mandiri operates with a focus on life insurance, general insurance and asset management through a variety of distribution channels under PT AXA Financial Indonesia, PT AXA Life Indonesia, PT Asuransi AXA Mandiri, and PT AXA Asset Management Indonesia.
						 <!--:--><!--:id-->AXA Mandiri terdiri dari bisnis asuransi jiwa, yaitu PT AXA Mandiri Financial Services dan bisnis asuransi umum, yaitu PT Mandiri AXA General Insurance, yang keduanya merupakan perusahaan patungan antara PT Bank Mandiri (Persero) Tbk dan AXA Group.<!--:-->"); 
				?>
			</p>
			<?php if(get_field('axa_indonesia_group')): ?>
			<div class="group-tabs">
				<dl class="tabs" data-tab>
					<?php while(has_sub_field('axa_indonesia_group')): ?>
						<?php $string = str_replace(' ','', get_sub_field('title'));?>
				  		<dd class="" data-entity="<?php echo $string; ?>"><a href="#<?php echo $string;?>"><?php the_sub_field('title');?></a></dd>
				  	<?php endwhile;?>
				</dl>
				<div class="tabs-content">
					<?php while(has_sub_field('axa_indonesia_group')): ?>
					<?php $string = str_replace(' ','', get_sub_field('title'));?>
					  <div class="content" id="<?php echo $string;?>">
					  	<?php 
					   		$nama = get_sub_field("ceo"); 
					   		if($nama[0]['nama'] != ""): ?>					   
					    <div class="large-6 columns">
					    <?php else:?>
					    	<div class="large-12 columns">
						<?php endif;?>
					    	 <h4></span><?php the_sub_field('title');?></h4>
					    	<?php the_sub_field('content');?>
					    </div>

					    <?php if(get_sub_field('ceo')): ?>
					   	<?php 
					   		$nama = get_sub_field("ceo"); 
					   		if($nama[0]['nama'] != ""): ?>
						    <div class="large-6 columns">
						    	<div class="ceo-section bg-grey o-hidden radius-all-5">
						    		<?php while(has_sub_field('ceo')): ?>
						    			<div class="large-3 columns text-center">
						    				<img src="<?php the_sub_field('photo');?>">	
						    			</div>
						    			<div class="large-9 columns">
						    				<h4><?php the_sub_field('nama');?></h4>
						    				<h5><?php the_sub_field('jabatan');?></h5>					    				
						    			</div>
						    			<div class="description clearfix">
						    				<?php the_sub_field('deskripsi');?>
						    			</div>
						    		<?php endwhile;?>
						    	</div>
						    </div>
						    <?php endif;?>
						<?php endif;?>
					  </div>
				  	<?php endwhile;?>
				</div>
			</div>				
			<?php endif;?>
		</section><!--end axa group test-->

		<section id="axa-group" class="clearfix background-vector" >
			
			<div id="logo3"></div>
			<p><?php _e("<!--:en-->AXA Mandiri operates with a focus on life insurance, general insurance and asset management through a variety of distribution channels under PT AXA Financial Indonesia, PT AXA Life Indonesia, PT Asuransi AXA Mandiri, and PT AXA Asset Management Indonesia.
						 <!--:--><!--:id-->AXA Mandiri terdiri dari bisnis asuransi jiwa, yaitu PT AXA Mandiri Financial Services dan bisnis asuransi umum, yaitu PT Mandiri AXA General Insurance, yang keduanya merupakan perusahaan patungan antara PT Bank Mandiri (Persero) Tbk dan AXA Group.<!--:-->"); 
				?>
			</p>
			<?php if(get_field('axa_indonesia_group')): ?>
				<ul class="group clearfix">
					<?php while(has_sub_field('axa_indonesia_group')): ?>
					<li> 
						<h2><?php the_sub_field('title');?></h2>
						<?php the_sub_field('content');?>
					</li>
					<?php endwhile;?>
				</ul>
			<?php endif;?>
		</section><!--end axa group-->

		<section id="board-director" class="clearfix" style="display:none">
			<h3 class="text-center f-18">BOARD OF DIRECTORS</h3>
			<?php if(get_field('board_of_director')): ?>
				<ul class="bod-section clearfix" style="margin:0;">
					<?php while(has_sub_field('board_of_director')): ?>
					<div class="filter <?php the_sub_field('entity'); ?>" data-entity2="<?php the_sub_field('entity'); ?>" style="display:none;">
						<li style="margin:10px;">
							<div class="panel bg-greydark o-hidden p-all-25 m-all-10 radius-all-5">
								<div class="large-3 columns p-left-0"><img src="<?php the_sub_field('image');?>"/></div>
								<div class="large-9 columns p-all-0 m-top-20">
									<h4><?php the_sub_field('name');?></h4>
									<h5><?php the_sub_field('jabatan');?></h5>
									<h6><?php echo get_sub_field_object('entity')['choices'][ get_sub_field('entity') ];  ?></h6>
								</div>
								<div class="details p-top-20" style="clear:both;">
									<p><?php the_sub_field('details');?></p>
								</div>
							</div> 
						</li>
					</div>
					<?php endwhile;?>
				</ul>
			<?php endif;?>
		</section><!--end board of director test-->

		<section id="karir-section" class="clearfix">
			<h3 class="small-title"><?php _e("<!--:en-->CAREER<!--:--><!--:id-->KARIR<!--:-->"); ?></h3>
			<div class="large-6 column first-child">
			<h4><?php _e("<!--:en-->Be Part of AXA Mandiri <!--:--><!--:id-->Jadi Bagian dari AXA Mandiri <!--:-->"); ?></h4>
				<p><?php _e("<!--:en-->To be the company of choice for employees, AXA Mandiri always create a comfortable working environment and ensures that each employee obtain various facilities to improve their competence. AXA Mandiri improve employee engagement success led the company won the award for Best Employer in Indonesia 2011 from Aon Hewitt Consulting Asia Pacific.<!--:-->
							<!--:id-->Untuk menjadi perusahaan pilihan bagi karyawan, AXA Mandiri senantiasa menciptakan lingkungan kerja yang nyaman dan memastikan bahwa setiap karyawan memperoleh berbagai fasilitas untuk meningkatkan kompetensi mereka. Keberhasilan AXA Mandiri meningkatkan employee engagement membawa perusahaan meraih penghargaan sebagai Best Employer in Indonesia 2011 dari Aon Hewitt Consulting Asia Pacific.<!--:-->"
				); ?></p>
				<a href="<?php echo  site_url('karir');?>" class="button">
					<?php _e("<!--:en-->Join with AXA Mandiri<!--:--><!--:id-->Bergabung dengan AXA Mandiri<!--:-->"); ?></a>
			</div>
			<div class="large-5 column">
				<ul class="career-list">
					<li class="world"><span><h6>Miliki kesempatan memimpin dalam perusahaan bertaraf internasional</h6></span></li>
					<li class="innovation"><span><h6>Ciptakan produk-produk inovatif dan pelayanan yang lebih baik</h6></span></li>
					<li class="environments"><span><h6>Bekerja di lingkungan yang terbuka terhadap pengembangan diri Anda</h6></span></li>
					<li class="ladder"><span><h6>Miliki kesempatan bekerja dan belajar serta mengembangkan karir</h6></span></li>
				</ul>
			</div>
		</section><!--end karir-->

		<section id="kontak" class="bg-white3 sections clearfix relative o-hidden">
			<!-- <div class="transparent-separate absolute" style="left:-145px;top:0;"><img src="<?php bloginfo('template_url'); ?>/images/separate-transparent.png"/></div> -->
			<!-- <a href="" class="button floating">Download Company Profile</a> -->
			<!-- <h3 class="small-title"></h3> -->
			<h4><?php _e("<!--:en-->Financial Statements<!--:--><!--:id-->Laporan Keuangan<!--:-->"); ?></h4>

			
	        <div class="large columns">
	        		<div class="bg-greydark clearfix large-5 columns radius-all-5" style="margin-bottom: 10px;">
	        			<div class="box p-all-10">
	        				<h5>Laporan Triwulan</h5>
	        				<form id="laporan-tahunan" action="<?php echo get_permalink(3326) ?>" method="GET">
								<?php 		
									$query = "SELECT YEAR(post_date) AS `year` FROM $wpdb->posts WHERE post_type = 'laporan_keuangan' GROUP BY YEAR(post_date) ORDER BY post_date DESC";
									$results = $wpdb->get_results($query);
									$currentYear = $results[0]->year;
									?>			
										
									<select id="laporan-tahun" class="required w-48p  columns" name="fyear">
										<option disabled selected value="">Pilih Tahun</option>
									<?php
									foreach($results as $result){
									$selected = ($_GET['fyear'] == $result->year) ? 'selected' : '';
									?>		
											<option value="<?php echo $result->year; ?>"><?php echo $result->year; ?></option>
									<?php
												}
									?>		
								</select>

								<select id='periode-keuangan' class='required right w-48p columns' name='fperiode'>
									<option disabled selected >Pilih Tipe periode</option>
									<option value="triwulan-i">Triwulan I</option>
									<option value="triwulan-ii">Triwulan II</option>
									<option value="triwulan-iii">Triwulan III</option>
									<option value="triwulan-iv">Triwulan IV</option>
									<option value="tahunan">Tahunan</option>
									
								</select>
								<!-- <select id="periode-keuangan" class="required w-48p right columns">
									<option disabled selected>Pilih Periode</option>

								</select> -->

								<div class="clearfix"></div>

								<?php
									$args2 = array(
										'hide_empty'               => 0,
										'hierarchical'             => 1,
										'taxonomy'                 => 'jenis-laporan'); 
								  $jenislaporan = get_categories($args2);
								  
								  $select = "<select id='laporan-keuangan' class='required w-48p columns' name='fjenis'>";
								  $select.= "<option disabled selected>Jenis Laporan Keuangan</option>";
								  
								  foreach($jenislaporan as $jenis){
								    if($jenis->count > 0){
								        $select.= "<option value='".$jenis->slug."'>".$jenis->name."</option>";
								    }
								  }
								  
								  $select.= "</select>";
								  
								  echo $select;
								?>
								<!-- <select id="jenis-keuangan" class="required w-48p columns">
									<option disabled selected>Jenis Laporan Keuangan</option>
								</select> -->
								

								<?php
									$args = array(
										'hide_empty'               => 0,
										'hierarchical'             => 1,
										'taxonomy'                 => 'laporan_entity'); 
								  $categories = get_categories($args);
								  
								  $select = "<select id='laporan-entity' class='required right w-48p columns' name='fentity'>";
								  $select.= "<option disabled selected>Pilih Tipe Asuransi</option>";
								  
								  foreach($categories as $category){
								    if($category->count > 0){
								        $select.= "<option value='".$category->slug."'>".$category->description."</option>";
								    }
								  }
								  
								  $select.= "</select>";
								  
								  echo $select;
								?>
								<!-- <small class="form-requirements">* Semua field wajib diisi.</small> -->
		        				<button type="submit" class="button right">Cari</button>
	        				</form>
	        			</div>
	        		</div>
	        		<div class="large-7 columns">
	        		<!-- Laporan Kinerja Bulanan -->
	        		<div class="large-6 columns" style="">
	        			<div class="bg-greydark clearfix   radius-all-5 box p-all-20">
	        				<h5>Laporan Kinerja Bulanan</h5>
	        				<form id="kinerja-bulanan" action="<?php echo site_url('kinerja-bulanan');?>">
					        	<select id="kinerja-bulan" name='fmonth'>
									<option>Pilih Bulan </option>
									<option value="1"> Januari </option>
									<option value="2"> Februari </option>
									<option value="3"> Maret </option>
									<option value="4"> April </option>
									<option value="5"> Mei </option>
									<option value="6"> Juni </option>
									<option value="7"> Juli </option>
									<option value="8"> Agustus </option>
									<option value="9"> September </option>
									<option value="10"> Oktober </option>
									<option value="11"> November </option>
									<option value="12"> Desember </option>
								</select>

								<?php 		
									$query = "SELECT YEAR(post_date) AS `year` FROM $wpdb->posts WHERE post_type = 'laporan_bulanan' GROUP BY YEAR(post_date) ORDER BY post_date DESC";
									$results = $wpdb->get_results($query);
									$currentYear = $results[0]->year;
									?>			
										
									<select id="kinerja-tahun"  disabled="disabled" name="fyear">
										<option>Pilih Tahun</option>
									<?php
									foreach($results as $result){
									$selected = ($_GET['fyear'] == $result->year) ? 'selected' : '';
									?>		
											<option value="<?php echo get_post_type_archive_link( 'media' ).$result->year; ?>"><?php echo $result->year; ?></option>
									<?php
												}
									?>		
								</select>

		        				<button type="submit" class="button right">Cari</button>
	        				</form>
	        			</div>
	        		</div>
	        		<!-- end Laporan Kinerja Bulanan -->

	        		<div class="large-6 columns">
	        			<div class="bg-greydark clearfix radius-all-5 box p-all-20">
	        				<h5>Laporan Tahunan</h5>
	        				<form id="kinerja-bulanan" action="<?php echo site_url('laporan-kinerja-bulanan');?>">
					        	<!-- <select id="kinerja-bulan">
									<option>Pilih Bulan </option>
									<option value="fmonth=1"> Januari </option>
									<option value="fmonth=2"> Februari </option>
									<option value="fmonth=3"> Maret </option>
									<option value="fmonth=4"> April </option>
									<option value="fmonth=5"> Mei </option>
									<option value="fmonth=6"> Juni </option>
									<option value="fmonth=7"> Juli </option>
									<option value="fmonth=8"> Agustus </option>
									<option value="fmonth=9"> September </option>
									<option value="fmonth=10"> Oktober </option>
									<option value="fmonth=11"> November </option>
									<option value="fmonth=12"> Desember </option>
								</select> -->

								<?php 		
									$query1 = "SELECT YEAR(post_date) AS `year` FROM $wpdb->posts WHERE post_type = 'kinerja_bulanan' GROUP BY YEAR(post_date) ORDER BY post_date DESC";
									$results1 = $wpdb->get_results($query1);
									$currentYear1 = $results1[0]->year;
									?>			
										
									<select id="kinerja-tahun" name="fyear">
										<option>Pilih Tahun</option>
									<?php
									foreach($results1 as $result1){
									$selected1 = ($_GET['fyear'] == $result1->year) ? 'selected' : '';
									?>		
											<option value="<?php echo $result1->year; ?>"><?php echo $result1->year; ?></option>
									<?php
												}
									?>		
									</select>

								<?php
									$args1 = array(
										'hide_empty'               => 0,
										'hierarchical'             => 1,
										'taxonomy'                 => 'laporan_entity',

									); 
								  $categories1 = get_categories($args1);
								  
								  $select1 = "<select id='laporan-entity' name='fentity'>";
								  $select1.= "<option  disabled selected>Pilih Tipe Asuransi</option>";
								  
								  foreach($categories1 as $category1){
								    if($category1->count > 0){
								        $select1.= "<option value='".$category1->slug."'>".$category1->description."</option>";
								    }
								  }
								  
								  $select1.= "</select>";
								  
								  echo $select1;
								?>
		        				<button type="submit" class="button right">Cari</button>
	        				</form>
	        			</div>
	        		</div>
	        	</div><!--end large 6-->
	       <!-- <div class="large-4 columns branch">
	                <strong><?php _e("<!--:en-->Branch Office<!--:--><!--:id-->Kantor Cabang<!--:-->"); ?></strong>
            		<p><?php _e("<!--:en-->Find the nearest AXA Mandiri branch office in the your city<!--:--><!--:id-->Cari kantor cabang AXA terdekat di kota Anda<!--:-->"); ?></p>
        		<select id="kantor-cabang">
					<option>Pilih Kota</option>
				</select>
				<button type="input" class="button red">Cari</button>
	        </div>-->
		</section><!--end section contact us-->

		<?php get_template_part("widget/breadcrumbs");?>
	</div><!--end row-->
<?php get_template_part("widget/hargaunit");?>
</div><!--end page-container-->

<?php get_footer();?>
<script type="text/javascript">
	$(document).ready(function(){
		$(".group-tabs dd:first-child").addClass("active");
		$(".tabs-content .content:first-child").addClass("active");
		var en=$('dd:first-child').data('entity');
		console.log(en);
		$("#board-director .owl-item .filter."+en).addClass('active');
		$("div.filter."+en).show();
	});
		$('dd').click(function(){
			$("#board-director .owl-item").removeClass('active');
			var href=$(this).data('entity');
			$( "div.filter" ).not( $('.'+href)).hide();
			$("div.filter."+href).addClass('active');
			$("div.filter."+href+".active").show();

			$("#board-director .owl-item .filter."+href).each(function() {
				var entity=$("#board-director .owl-item .active").attr('class');
				var en_arr=entity[1];
				console.log(en_arr);
			 
			});
		});
</script>
