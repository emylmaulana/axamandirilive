<footer id="footer" class="">
	<div class="row">
        <div id="link-cepat-footer" class="large-6  columns clearfix">
        	<div class="footer-menu">
        	<h5><?php _e("<!--:en-->Quick Menu<!--:--><!--:id-->Link Cepat<!--:-->"); ?></h5>
              <?php $quickmenu = array(
                        'theme_location'  => '',
                        'container'       => '',
                        'menu'            => 'Link Cepat',
                        'echo'            => true,
                        'fallback_cb'     => 'wp_page_menu',
                        'items_wrap'      => '<ul class="%2$s clearfix">%3$s</ul>',
                        'depth'           => 0,
                    );
                wp_nav_menu( $quickmenu );?>
                <?php $linkcepat2 = array(
                        'theme_location'  => '',
                        'container'       => '',
                        'menu'            => 'Link Cepat 2',
                        'echo'            => true,
                        'fallback_cb'     => 'wp_page_menu',
                        'items_wrap'      => '<ul class="%2$s clearfix">%3$s</ul>',
                        'depth'           => 0,
                    );
                wp_nav_menu( $linkcepat2 );?>
            </div>
        </div>
        <div id="customer-care-footer" class="large-3 columns">
            <div class="main-sites right m-top-15">
                <p id="footer-customer">Customer Care Centre</p>
                <p class="headerphone">+62 21 3005 8788</p>
                <p id="temukan">Temukan Kami</p>
            </div>
             <div id="social-footer" class="clearfix" style="clear:both;">
                <?php if(get_field('axa_mandiri_socmed', 'options')): ?>
                    <?php while(has_sub_field('axa_mandiri_socmed', 'options')): ?>
                        <div class="social-icons">
                            <a href="<?php the_sub_field('youtube'); ?>" class="icons youtube"><i class="fa fa-youtube-play"></i></a>
                            <a href="<?php the_sub_field('facebook'); ?>" class="icons facebook"><i class="fa fa-facebook"></i></a>
                            <a href="<?php the_sub_field('twitter'); ?>" class="icons twitter"><i class="fa fa-twitter"></i></a>
                        </div>
                    <?php endwhile;?>
                <?php endif;?>
            </div>
        </div>
	</div>
</footer>

<footer id="footer-copyright">
    <p><a href="<?php echo site_url();?>/disclaimer" style="color:#ffd200" target="_blank">Disclaimer &amp; Ownership.</a> Copyright 2014 AXA Mandiri Financial Services</p>

</footer>

    <div id="customercare-fixed" class="">
            <?php get_template_part('widget/customer-care');?>
    </div>
    <a class="exit-off-canvas"></a>
  </div>
</div><!--end offcanvas-->
   <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=656866527686942";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>


    <?php wp_footer();?>
    <!--[if lt IE 9]>
       <script type="text/javascript">
        jQuery(document).ready(function() {
        jQuery(".popup_box").fancybox({ 
            margin      : 0,
            maxWidth    : 300,
            maxHeight   : 315,
            fitToView   : true,
            scrolling   : "no",
            padding: 0,
            }).trigger('click');
        });
        </script>

        <div class="popup_box" style="display:none;text-align:center;background:#EEE;color:#153389;">
            <h3 style="background:#fff;font-size:1.250em;line-height:1.2;padding:20px; 0;margin:0;color:#153389;">Selamat Datang di Website AXA Indonesia</h3>
            <p style="margin-bottom:0; padding:20px;">Mohon untuk melakukan upgrade<br>
                pada web browser Anda<br>
                untuk tampilan optimal.</p>
        </div> 
    <![endif]-->
  </body>
</html>