<?php/** * Template Name: Homepage */?>
<?php get_header();?>
<span style="display:none"><h1>AXA Mandiri</h1></span>
<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=569851&mt_adid=125263&v1=&v2=&v3=&s1=&s2=&s3='></script>
	<div id="mainSlider">
		<div class="row box">
			<div class="large-11">
				<ul id="separate" class="personal">
					<li class="selected"><a href="<?php echo site_url('home');?>">Personal</a></li>
					<li><a href="<?php echo site_url('bisnis');?>">Bisnis</a></li>
				</ul>	
				<div class="show-for-large-only"><?php get_template_part("widget/customer-care");?></div>
			</div>
		</div>
		<ul id="slideContent" class="owl-carousel hide-on-tablet">
			<?php 
				$args = array("post_type" => "slide_personal",
							"posts_per_page" =>8,
							'tax_query'=>array(
													array('taxonomy'=>"tipe_layar",
															'field'=>'slug',
															'terms'=>'desktop'
															)
													) );
				query_posts( $args );
				if(have_posts()): while(have_posts()):the_post();
			?>

			<?php if(get_field('video_slide')):?>
			<li class="relative">
				<div class="slideBG">
					 <video class="video-bg" width="100%" loop autoplay>
					    <source src="<?php the_field('video_slide');?>" type="video/webm">
					    <source src="<?php the_field('video_mp4');?>" type='video/mp4'/>
					    <source src="<?php the_field('video_ogv');?>" type="video/ogg"/>
					  </video>
					<div class="row">
						<div class="caption">
							<h3 style="color:<?php the_field('color_picker'); ?>"><?php the_title();?></h3>
							<!-- <a href="<?php the_field("slide_url");?>" class="playvideo buttons" style="border-color:<?php the_field('color_picker'); ?>;color:<?php the_field('color_picker'); ?>"> <i style="color:<?php the_field('button_color'); ?>;background:<?php the_field('color_picker'); ?>" class="fa fa-chevron-right"></i> <?php the_field("slide_button_text");?></a> -->
							<?php $slide_url = get_field("slide_url"); ?>
							<a href="<?=(isset($slide_url) && $slide_url != "") ? $slide_url : 'javascript:void(0);'?>" class="playvideo buttons" style="border-color:<?php the_field('color_picker'); ?>;color:<?php the_field('color_picker'); ?>"> <i style="color:<?php the_field('button_color'); ?>;background:<?php the_field('color_picker'); ?>" class="fa fa-chevron-right"></i> <?php the_field("slide_button_text");?></a>
						</div>
					</div>
				</div>
				<iframe id="ytvideo" style="display:none" width="100%" height="100%" src="https://www.youtube.com/embed/<?php the_field('slide_video_url');?>?enablejsapi=1&version=3&playerapiid=ytvideo?rel=0&controls=1&fs=0&modestbranding=1&showinfo=0" frameborder="0" wmode="Opaque" allowfullscreen></iframe>

				<a href="#" class="closeVideo" style="display:none;">X</a>
			</li>
			<?php elseif(get_field('slider_image')):?>
			<li class="relative">
				<a href="<?php the_field("slide_url");?>">
					<div class="slideBG" style="background-image:url(<?php the_field('slider_image');?>);">
						<div class="row">
							<div class="caption">
								<h3 style="color:<?php the_field('color_picker'); ?>"><?php the_title();?></h3>
								<?php if(get_field('slide_button_text')!=''):?>
									<span class="buttons" style="border-color:<?php the_field('color_picker'); ?>;color:<?php the_field('color_picker'); ?>"> <i style="color:<?php the_field('button_color'); ?>;background:<?php the_field('color_picker'); ?>" class="fa fa-chevron-right"></i> <?php the_field("slide_button_text");?></span>
								<?php endif;?>
							</div>
						</div>
					</div>
				</a>
			</li>
			<?php endif;?>
			<?php endwhile;?>
		</ul>
		<?php endif;?>

		<ul id="mobile-slide" class="tablet-content owl-carousel">
			<?php 
				$args = array("post_type" => "slide_personal",
							"posts_per_page" =>8,
							'tax_query'=>array(
													array('taxonomy'=>"tipe_layar",
															'field'=>'slug',
															'terms'=>'mobile'
															)
													) );
				query_posts( $args );
				if(have_posts()): while(have_posts()):the_post();
			?>
			<?php if(get_field('video_slide')):?>
			<li class="relative">
				
				<div class="slideBG"  style="background-image:url(<?php the_field('slider_image');?>);">
					<div class="video-bg"></div>
					<div class="row">
						<div class="caption">
							<h3 style="color:<?php the_field('color_picker'); ?>"><?php the_title();?></h3>
							<?php $slide_url = get_field("slide_url"); ?>
							<a href="<?=(isset($slide_url) && $slide_url != "") ? $slide_url : 'javascript:void(0);'?>" class="playvideo buttons" style="border-color:<?php the_field('color_picker'); ?>;color:<?php the_field('color_picker'); ?>"> <i style="color:<?php the_field('button_color'); ?>;background:<?php the_field('color_picker'); ?>" class="fa fa-chevron-right"></i> <?php the_field("slide_button_text");?></a>
						</div>
					</div>
				</div>
				<iframe id="ytvideo-mobile" style="display:none" width="100%" height="100%" src="https://www.youtube.com/embed/<?php the_field('slide_video_url');?>?enablejsapi=1&version=3&playerapiid=ytvideo-mobile&rel=0&controls=1&fs=0&modestbranding=1&showinfo=0&autoplay=0" frameborder="0" wmode="Opaque" allowfullscreen></iframe>
				<a href="#" class="closeVideo" style="display:none;">X</a>
			</li>
			<?php elseif(get_field('slider_image')):?>
			<li class="relative">
				<a href="<?php the_field("slide_url");?>">
					<div class="slideBG"  style="background-image:url(<?php the_field('slider_image');?>);">
						<div class="row">
							<div class="caption">
								<h3 style="color:<?php the_field('color_picker'); ?>"><?php the_title();?></h3>
								<?php if(get_field('slide_button_text')!=''):?>
								<span class="buttons" style="border-color:<?php the_field('color_picker'); ?>;color:<?php the_field('color_picker'); ?>"> <i style="color:<?php the_field('button_color'); ?>;background:<?php the_field('color_picker'); ?>" class="fa fa-chevron-right"></i> <?php the_field("slide_button_text");?></span>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</a>
			</li>
			
			<?php endif;?>
			<?php endwhile;?>
			
		</ul>
		<?php endif;?>
	</div>


	<div id="solusi_main" class="bg-greylight">
		<div class="row solusiContainer">
			<div id="solusiWidget">
				<h2 class="m-bottom-30"><?php _e("<!--:en-->Our Solution<!--:--><!--:id-->Solusi Kami<!--:-->"); ?></h2>
				<ul class="clearfix large-block-grid-3 medium-block-grid-3 small-block-grid-1">
					<?php if(get_field('personal_page', 'options')): ?>
						<?php while(has_sub_field('personal_page', 'options')): ?>
							<?php if(get_sub_field('solusi_kami', 'options')): ?>
								<?php while(has_sub_field('solusi_kami', 'options')): ?>
								<?php if(get_sub_field('text_image')=='KESEHATAN'): ?>
									<?php $label_ga='Solusi kesehatan personal button'; ?>
								<?php elseif(get_sub_field('text_image')=='PROTEKSI'): ?>
									<?php $label_ga='Solusi proteksi personal button'; ?>
								<?php elseif(get_sub_field('text_image')=='UMUM'): ?>
									<?php $label_ga='Solusi umum personal button'; ?>
								<?php else: ?>
									<?php $label_ga=''; ?>
								<?php endif; ?>
								<li>
									<a href="<?php the_sub_field('url'); ?>" onclick="ga('send', 'event', 'button', 'click','<?php echo $label_ga;?>');">
										<div id="solusi-kesehatan" class="img_background" style="background-image:url(<?php the_sub_field('image'); ?>)">
										<!-- <a href="<?php the_sub_field('url'); ?>" class="clearfix"> -->
											<!-- <span class="bg-cover-left-solusiindex"></span> -->
											<h2 class="title"><?php the_sub_field('text_image'); ?></h2>
											<span class="icon-solusi-index" style="background-image:url(<?php the_sub_field('icon'); ?>)"></span>
											<span class="bg-cover-right-solusiindex"></span>
									</a>
									</div>
									<p class="show-for-large-only"><?php the_sub_field('excerpt'); ?></p>
								<a href="<?php the_sub_field('url'); ?>" onclick="ga('send', 'event', 'button', 'click',<?php echo $label_ga;?>);">
									<div class="solusi_button">
										<span class="f-14">
											<i class="fa fa-chevron-circle-right"></i><?php the_sub_field('button'); ?>
										</span>
									</div>
								</a>
								</li>
								<?php endwhile; ?>
							<?php endif; ?>
						<?php endwhile; ?>
					<?php endif; ?>
					<!-- <li class="large-4 columns"></li>
					<li class="large-4 columns"></li> -->
				</ul>
			</div>
		</div>
	</div>

	<div id="sliderBanner" class="white p-all-30">
		<div class="row">
			<div class="box_slide">
				<ul id="main_slidebanner" class="clearfix">
					<?php 
						$args = array("post_type" => "slide_banner","posts_per_page" =>5);
						query_posts( $args );
						if(have_posts()): while(have_posts()):the_post();
					?>
					<li class="box_images" class="large-3 columns">
						<div class="box_slide">
							<a href="<?php the_field("slide_url") ?>"><img class="img_banner" src="<?php the_field('slider_image');?>" alt="img-banner"></a>
						</div>
					</li>
					<?php endwhile;?>
				</ul>
				<?php endif;?>
			</div>
		</div>
	</div>
		<div id="advisor-teaser">
		<div class="row">
			<div class="large-6 columns">
				<h3><span></span>Solusi untuk Anda</h3>
				<p>Cari tahu jenis perlindungan sesuai kebutuhan Anda</p>	
			</div>
			<div class="large-5 columns">
				<form action="<?php echo site_url('solution-advisor'); ?>" method='GET'>
					<label>Tahun ini saya berumur</label>
					<input type="text" id="age" value="25"/>
					<input type="submit" value="Mulai" class="button red"/>
					<input type="hidden" name="umur" id="hasil-age" value=""/>
				</form>
			</div>
		</div>	
		<script type="text/javascript">
			$(document).ready(function(){
				
				$("#age").keyup(function(){
					var age = $("#age").val();
					if (age>=1 && age<=25) {
						$("#hasil-age").attr("value", "20");
					}else if (age>=26 && age<=40) {
						$("#hasil-age").attr("value", "30");
					}else if (age>=41 && age<=55) {
						$("#hasil-age").attr("value", "50");
					}else {
						$("#hasil-age").attr("value", "57");
					};
				}) .keyup();
				});
		</script>
	</div>

	<?php get_template_part("widget/hargaunit");?>


	<div id="layanan" class="background-vector">
		<div class="row">
			<div class="large-11 column centered">
				<h2><?php _e("<!--:en-->SERVICE IN YOUR HAND<!--:--><!--:id-->LAYANAN DI TANGAN ANDA<!--:-->"); ?></h2>
				<ul class="clearfix">
					<?php if(get_field('layanan_home', 'options')): ?>
						<?php while(has_sub_field('layanan_home', 'options')): ?>
							<?php if(get_sub_field('layanan_anda_personal', 'options')): ?>
								<?php while(has_sub_field('layanan_anda_personal', 'options')): ?>
									<li class="large-4 columns premi">
										<div class="icon" style="background-image:url(<?php the_sub_field('image'); ?>)"></div>
										<div class="details">
											<h2><?php the_sub_field('title'); ?></h2>
											<p class="show-for-large-only"><?php the_sub_field('excerpt'); ?></p>
											<a href="<?php the_sub_field('url'); ?>" class="button blue"><?php the_sub_field('button'); ?></a>
										</div>
									</li>
								<?php endwhile; ?>
							<?php endif; ?>
						<?php endwhile; ?>
					<?php endif; ?>
				</ul>
			</div>
		</div><!--end row-->
	</div><!--end layanan-->
	<?php echo do_shortcode('[widget_inspirasi][/widget_inspirasi]'); ?>
	<div id="region-1">
		<div class="row">
			<!-- <div class="large-11 large-centered columns show-for-large-only"> -->
				<div class="large-4 columns">
					<?php get_template_part("widget/berita-terbaru");?>
				</div>
				<div class="large-4 columns">
					<?php get_template_part("widget/footer-direktori");?>
				</div>
				<div class="large-4 columns">
					<?php get_template_part("widget/karir");?>
				</div>
			<!-- </div> -->
		</div><!--end row-->
	</div><!--end region 1-->

<?php get_footer();?>