<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title><?php bloginfo('name'); ?><?php wp_title('|',true,''); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta content="definite.co.id" name="author">
        <?php
        description_insert('');// addding meta description
        keyword_insert('');//adding meta keyword
        ?>
        
        <!-- CSS Styles  -->
        <link href="<?php bloginfo('template_url');?>/css/normalize.css" rel="stylesheet">
        <link href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" rel="stylesheet">

        <link href="<?php bloginfo('template_url');?>/css/mobile.css" rel="stylesheet">
        
        <!-- Favicon and touch icons  -->
        <link href="<?php bloginfo('template_url');?>/favicon.png" rel="shortcut icon">
         
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
      	
    	<![endif]-->

        <script src="//www.google.com/jsapi?key=AIzaSyCe8_LRA-Qf-w6WvnRYleuAO3V-BfDMJLg"></script>
       
       

        <?php wp_head();?>
        <script type="text/javascript">
            var bloginfo = '<?php bloginfo('template_url'); ?>';
            var baseurl = '<?php echo site_url();?>'
        </script>
        <?php _e("<!--:en--><script src='https://maps.google.com/maps/api/js?sensor=true&amp;language=en' type='text/javascript'></script><!--:-->
    <!--:id--><script src='https://maps.google.com/maps/api/js?sensor=true&amp;language=id' type='text/javascript'></script><!--:-->"); ?>
    <script type='text/javascript' src='<?php bloginfo('template_directory') ?>/js/vendor/markerclusterer.js'></script>

    <script src="<?php bloginfo('template_directory') ?>/js/vendor/gmap3.min.js" type="text/javascript" ></script>

  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-33864921-9', 'auto');
    ga('send', 'pageview');

</script>    

    </head>
    <body <?php body_class($class); ?>> 

    <div class="off-canvas-wrap">
    <div class="inner-wrap">

    <header id="header" class="desktop-content">
        <div class="row">
                <div id="topSection" class="clearfix">
                    <div id="menuLeft" class="left">
                        <div id="left_menu">
                            <ul class="clearfix">
                                <li><a href="<?php echo site_url('hubungi-kami');?>">Hubungi kami</a></li>
                                <li><a href="<?php echo site_url('media');?>">Media</a></li>
                                <li><a href="<?php echo site_url('karir');?>">Karir</a></li>
                                <!-- <li><?php echo qtrans_generateLanguageSelectCode('both'); ?></li> -->
                            </ul>

                        </div> 

                        <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
                            <input type="text" value="" name="s" id="s" placeholder="<?php _e("<!--:en-->Search...<!--:--><!--:id-->Cari...<!--:-->"); ?>" />
                            <button type="submit" id="searchsubmit"></button>
                        </form>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                    </div>

                    <div id="logo" class="right"><a href="<?php echo site_url('');?>">AXA Mandiri</a></div>

                </div>
            </div>
        </div><!--end row-->
        
        <div id="main-navigation">
             <div class="row">
                <?php $mainmenu = array(
                        'theme_location'  => 'primary_menu',
                        'container'       => 'nav',
                        'echo'            => true,
                        'fallback_cb'     => 'wp_page_menu',
                        'items_wrap'      => '<ul id="mainmenu" class="%2$s main-nav clearfix">%3$s</ul>',
                        'depth'           => 0,
                        'walker'         => new axa_sublevel
                    );
                wp_nav_menu( $mainmenu );?>

           
                   <!--<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
                            <input type="text" value="" name="s" id="s" placeholder="<?php _e("<!--:en-->Search...<!--:--><!--:id-->Cari...<!--:-->"); ?>" />
                            <button type="submit" id="searchsubmit"></button>
                        </div>
                    </form> -->
                                <div id="solution-nav">
                <a href="<?php echo site_url('solution-advisor'); ?>" class="btn-primary">Solusi untuk Anda <i class="fa fa-angle-right"></i></a>
            </div>

            </div><!--end row-->
        </div><!--end main navigation-->
    </header><!--end header-->

    <div id="fixed-navigation" style="display:none;">
        <div class="row">
            <?php $mainmenu = array(
                        'theme_location'  => 'primary_menu',
                        'container'       => 'nav',
                        'echo'            => true,
                        'fallback_cb'     => 'wp_page_menu',
                        'items_wrap'      => '<ul id="fixedmenu" class="%2$s main-nav clearfix">%3$s</ul>',
                        'depth'           => 0,
                        'walker'         => new axa_sublevel
                    );
                wp_nav_menu( $mainmenu );?>
                <ul class="other">
                    <li class="search-menu"><a href="javascript:void(0)"><span style="display:none">search menu</span></a></li>
                    <li id="container-search-wrap"  style="display:none;">
                        <div id="search-wrap" class="clearfix">
                            <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
                                <input type="text" value="" name="s" id="s" placeholder="<?php _e("<!--:en-->Search...<!--:--><!--:id-->Cari...<!--:-->"); ?>" />
                            </form>
                        </div>
                    </li>
                    <li class="logo-fix-nav"><a href="<?php echo site_url('');?>"><span style="display:none">logo nav</span></a></li>
                </ul>

        </div>

    </div>

    <div class="off-canvas-wrap">
    <div class="inner-wrap">


<div id="mobile-navigation"  class="mobile-content">
        <nav class="tab-bar">
          <section class="left-small">
            <a class="left-off-canvas-toggle menu-icon" ><span></span></a>
          </section>

          <section class="middle tab-bar-section">
                <span id="logo"><a href="<?php echo site_url('');?>" class="block">AXA Mandiri</a></span>
          </section>

         <section class="right-small">
            <a class="right-off-canvas-toggle" ><i class="fa fa-phone"></i></a>
          </section>
        </nav>

        <aside class="left-off-canvas-menu">
          <ul class="off-canvas-list">
            <div class="dark">
                <li class="menu-item home"><a href="<?php echo site_url('');?>">Home</a></li>
                <li class="menu-search">   <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
                                <input type="text" value="" name="s" id="s" placeholder="<?php _e("<!--:en-->Search...<!--:--><!--:id-->Cari...<!--:-->"); ?>" />
                                <button type="submit" id="searchsubmit"></button>
                    </form>
                </li>
                <li class="menu-item parent-menu"><a href="#">Solusi Kami <span class="arrow"><i class="fa fa-angle-down arrow-down"></i> <i class="fa fa-angle-up arrow-up"></i></span></a>
                    <ul class="sub-menu-first">
                        <li class="sub-parent"><a href="#"><i class="fa fa-angle-down arrows-down"></i> <i class="fa fa-angle-up arrows-up"></i> Personal</a>
                            <ul class="sub-menu-second">
                                <li class="kesehatan"><a href="<?php echo site_url('solusi-kesehatan'); ?>"><span></span>Kesehatan</a></li>
                                <li class="kesehatan"><a href="<?php echo site_url('solusi-proteksi'); ?>"><span></span>Proteksi</a></li>
                                <li class="kesehatan"><a href="<?php echo site_url('solusi-umum'); ?>"><span></span>Umum</a></li>
                            </ul>
                        </li>
                        <li class="sub-parent"><a href="#"><i class="fa fa-angle-down arrows-down"></i> <i class="fa fa-angle-up arrows-up"></i> Bisnis</a>
                            <ul class="sub-menu-second">
                                <li class="kesehatan"><a href="<?php echo site_url('bisnis/solusi-kesehatan'); ?>"><span></span>Kesehatan</a></li>
                                <li class="kesehatan"><a href="<?php echo site_url('bisnis/solusi-proteksi'); ?>"><span></span>Proteksi</a></li>
                                <li class="kesehatan"><a href="<?php echo site_url('bisnis/solusi-umum'); ?>"><span></span>Umum</a></li>
                            </ul>
                        </li>
                    </ul><!--end sub menu first-->
                </li><!--end solusi kami-->
                <li class="menu-item"><a href="<?php echo site_url('layanan-nasabah');?>">Layanan Nasabah</a></li>
                <li class="menu-item"><a href="<?php echo site_url('direktori');?>">Direktori</a></li>
                <li class="menu-item"><a href="<?php echo site_url('tentang-axa-mandiri');?>">Tentang AXA Mandiri</a></li>
            </div>
            <li class="other-menu first-child"><a href="<?php echo site_url('hubungi-kami');?>">Hubungi Kami</a></li>
            <li class="other-menu"><a href="<?php echo site_url('media');?>">Media</a></li>
            <li class="other-menu"><a href="<?php echo site_url('karir');?>">Karir</a></li>
            <!-- <li class="other-menu"><?php# echo qtrans_generateLanguageSelectCode('both'); ?></li> -->
            <div id="solution-nav">
                <a href="<?php echo site_url('solution-advisor'); ?>" class="btn-primary">Solusi untuk Anda <i class="fa fa-angle-right"></i></a>
            </div>
        </ul><!--end off canvas-->
        </aside>

    <aside class="right-off-canvas-menu">
        <?php get_template_part('widget/customer-care');?>
    </aside>
</div>

