<?php
// kill the page when someone have unsufficient privilege
if( !current_user_can('manage_options') ) wp_die(__('You do not have sufficient permissions to import content in this site.'));

require_once('ulip.php');

$title = __('ULIP Rate');


if(!isset($_GET['action'])){
	$action = "add";
} else {
	$action = $_GET['action'];
}


if( $_GET['action']=="edit" && isset($_GET['id'])){
	
	global $wpdb;
	$ulipid = $_GET['id']; 
	$query = $wpdb->get_row("SELECT * FROM wp_ulip_rate WHERE id=$ulipid");

	$MaestroLink_Fixed_Income_Plus_IDR_BID = $query->MaestroLink_Fixed_Income_Plus_IDR_BID;
	$MaestroLink_Fixed_Income_Plus_IDR_OFFER = $query->MaestroLink_Fixed_Income_Plus_IDR_OFFER;
	$MaestroLink_Cash_Plus_IDR_BID = $query->MaestroLink_Cash_Plus_IDR_BID;
	$MaestroLink_Cash_Plus_IDR_OFFER = $query->MaestroLink_Cash_Plus_IDR_OFFER;
	$MaestroLink_Equity_Plus_IDR_BID = $query->MaestroLink_Equity_Plus_IDR_BID;
	$MaestroLink_Equity_Plus_IDR_OFFER = $query->MaestroLink_Equity_Plus_IDR_OFFER;
	$MaestroLink_Fixed_Income_Plus_USD_BID = $query->MaestroLink_Fixed_Income_Plus_USD_BID;
	$MaestroLink_Fixed_Income_Plus_USD_OFFER = $query->MaestroLink_Fixed_Income_Plus_USD_OFFER;
	$MaestroLink_Balanced_IDR_BID = $query->MaestroLink_Balanced_IDR_BID;
	$MaestroLink_Balanced_IDR_OFFER = $query->MaestroLink_Balanced_IDR_OFFER;
	// $MaestroPiece_Platinum_USD_BID = $query->MaestroPiece_Platinum_USD_BID;
	// $MaestroPiece_Platinum_USD_OFFER = $query->MaestroPiece_Platinum_USD_OFFER;
	// $MaestroPiece_Platinum_02_USD_BID = $query->MaestroPiece_Platinum_02_USD_BID;
	// $MaestroPiece_Platinum_02_USD_OFFER = $query->MaestroPiece_Platinum_02_USD_OFFER;
	$Maestro_Equity_Syariah_IDR_BID = $query->Maestro_Equity_Syariah_IDR_BID;
	$Maestro_Equity_Syariah_IDR_OFFER = $query->Maestro_Equity_Syariah_IDR_OFFER;
	$Maestro_Balance_Syariah_IDR_BID = $query->Maestro_Balance_Syariah_IDR_BID;
	$Maestro_Balance_Syariah_IDR_OFFER = $query->Maestro_Balance_Syariah_IDR_OFFER;
	$MaestroLink_Dynamic_IDR_BID = $query->MaestroLink_Dynamic_IDR_BID;
	$MaestroLink_Dynamic_IDR_OFFER = $query->MaestroLink_Dynamic_IDR_OFFER;
	$MaestroLink_Aggresive_Equity_IDR_BID = $query->MaestroLink_Aggresive_Equity_IDR_BID;
	$MaestroLink_Aggresive_Equity_IDR_OFFER = $query->MaestroLink_Aggresive_Equity_IDR_OFFER;
	$AFI_Dynamic_Money_IDR_BID = $query->AFI_Dynamic_Money_IDR_BID;
	$AFI_Dynamic_Money_IDR_OFFER = $query->AFI_Dynamic_Money_IDR_OFFER;
	$AFI_Progressive_Money_IDR_BID = $query->AFI_Progressive_Money_IDR_BID;
	$AFI_Progressive_Money_IDR_OFFER = $query->AFI_Progressive_Money_IDR_OFFER;
	$AFI_Secure_Money_IDR_BID = $query->AFI_Secure_Money_IDR_BID;
	$AFI_Secure_Money_IDR_OFFER = $query->AFI_Secure_Money_IDR_OFFER;
	$Secure_Money_USD_BID = $query->Secure_Money_USD_BID;
	$Secure_Money_USD_OFFER = $query->Secure_Money_USD_OFFER;
	$Money_Market_IDR_BID = $query->Money_Market_IDR_BID;
	$Money_Market_IDR_OFFER = $query->Money_Market_IDR_OFFER;
	$Syariah_Dynamic_BID = $query->Syariah_Dynamic_BID;
	$Syariah_Dynamic_OFFER = $query->Syariah_Dynamic_OFFER;
	$Syariah_Progressive_BID = $query->Syariah_Progressive_BID;
	$Syariah_Progressive_OFFER = $query->Syariah_Progressive_OFFER;
	$Maestro_Progressive_Equity_Syariah_BID = $query->Maestro_Progressive_Equity_Syariah_BID;
	$Maestro_Progressive_Equity_Syariah_OFFER = $query->Maestro_Progressive_Equity_Syariah_OFFER;
	$Maestrolink_Maxi_Advantage_BID = $query->Maestrolink_Maxi_Advantage_BID;
	$Maestrolink_Maxi_Advantage_OFFER = $query->Maestrolink_Maxi_Advantage_OFFER;
	$Maestrolink_Maxi_Advantage_GP = $query->Maestrolink_Maxi_Advantage_GP;
	$MaestroBerimbang_BID = $query->MaestroBerimbang_BID;
	$MaestroBerimbang_OFFER = $query->MaestroBerimbang_OFFER;
	$AXA_CitraDinamis_BID = $query->AXA_CitraDinamis_BID;
	$AXA_CitraDinamis_OFFER = $query->AXA_CitraDinamis_OFFER;
	$CitraGold_BID = $query->CitraGold_BID;
	$CitraGold_OFFER = $query->CitraGold_OFFER;
	$AXA_MaestroDollar_BID = $query->AXA_MaestroDollar_BID;
	$AXA_MaestroDollar_OFFER = $query->AXA_MaestroDollar_OFFER;
	$AXA_MaestroObligasi_Plus_BID = $query->AXA_MaestroObligasi_Plus_BID;
	$AXA_MaestroObligasi_Plus_OFFER = $query->AXA_MaestroObligasi_Plus_OFFER;
	$AXA_MaestroSaham_BID = $query->AXA_MaestroSaham_BID;
	$AXA_MaestroSaham_OFFER = $query->AXA_MaestroSaham_OFFER;
	$ALI_Dynamic_BID = $query->ALI_Dynamic_BID;
	$ALI_Dynamic_OFFER = $query->ALI_Dynamic_OFFER;
	$ALI_Proressive_BID = $query->ALI_Proressive_BID;
	$ALI_Proressive_OFFER = $query->ALI_Proressive_OFFER;
	$ALI_Secure_BID = $query->ALI_Secure_BID;
	$ALI_Secure_OFFER = $query->ALI_Secure_OFFER;



	$timestamp = strtotime($query->Date);	
	$day = date('d', $timestamp);
	$month = date('m', $timestamp);
	$year = date('Y', $timestamp);
}

?>


<div class="wrap">
	<h2><?php echo $title; ?></h2>
	<p><?php _e('This page is for adding ULIP rate'); ?></p>
	

	<?php 
		if(count($infos) > 0 ){
	?>
	<div class="infos">
	<?php			
			foreach ($infos as $value) {
				echo $value;
			}
	?>
	</div>
	<?php		
		}
	?>
	
	<form id="ulip-edit" class="form"name="ulip-edit" method="post" action="">
	<input type="hidden" name="action" value="<?php echo $action ?>"/>
	<input type="hidden" name="ulipid" value="<?php echo $ulipid ?>"/>
	<?php wp_nonce_field('ulip-add','ulip_nonce_field'); ?>
		<table width="100%" >
			<tr>
				<td><label for="Date">Date</label></td>
				<td>
					
					<div class="timestamp-wrap">
						<input type="text" id="jj" name="jj" value="<?php echo ($day) ? $day : date("d", time() - 60 * 60 * 24); ?>" size="2" maxlength="2" tabindex="4" autocomplete="off" style="text-align: center;">&nbsp;&nbsp;/&nbsp;
						<select id="mm" name="mm" tabindex="4">
							<option value="01">Jan</option>
							<option value="02">Feb</option>
							<option value="03">Mar</option>
							<option value="04">Apr</option>
							<option value="05">May</option>
							<option value="06">Jun</option>
							<option value="07">Jul</option>
							<option value="08">Aug</option>
							<option value="09">Sep</option>
							<option value="10">Oct</option>
							<option value="11">Nov</option>
							<option value="12">Dec</option>
						</select>
						
						&nbsp;/&nbsp;&nbsp; <input type="text" id="aa" name="aa" value="<?php echo ($year) ? $year : date("Y"); ?>" size="4" maxlength="4" tabindex="4" autocomplete="off"  style="text-align: center;">
					</div>					

				</td>

			</tr>
			<tr>
				<td><label for="MaestroLink_Fixed_Income_Plus_IDR">MaestroLink Fixed Income Plus (IDR)</label></td>
				<td>Bid: <input type="text" name="MaestroLink_Fixed_Income_Plus_IDR_BID" id="MaestroLink_Fixed_Income_Plus_IDR_BID" value="<?php echo $MaestroLink_Fixed_Income_Plus_IDR_BID; ?>" /></td>
				<td>Offer: <input type="text" name="MaestroLink_Fixed_Income_Plus_IDR_OFFER" id="MaestroLink_Fixed_Income_Plus_IDR_OFFER" value="<?php echo $MaestroLink_Fixed_Income_Plus_IDR_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="MaestroLink_Cash_Plus_IDR">MaestroLink Cash Plus (IDR)</label></td>
				<td>Bid: <input type="text" name="MaestroLink_Cash_Plus_IDR_BID" id="MaestroLink_Cash_Plus_IDR_BID" value="<?php echo $MaestroLink_Cash_Plus_IDR_BID; ?>" /></td>
				<td>Offer: <input type="text" name="MaestroLink_Cash_Plus_IDR_OFFER" id="MaestroLink_Cash_Plus_IDR_OFFER" value="<?php echo $MaestroLink_Cash_Plus_IDR_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="MaestroLink_Equity_Plus_IDR">MaestroLink Equity Plus (IDR)</label></td>
				<td>Bid: <input type="text" name="MaestroLink_Equity_Plus_IDR_BID" id="MaestroLink_Equity_Plus_IDR_BID" value="<?php echo $MaestroLink_Equity_Plus_IDR_BID; ?>" /></td>
				<td>Offer: <input type="text" name="MaestroLink_Equity_Plus_IDR_OFFER" id="MaestroLink_Equity_Plus_IDR_OFFER" value="<?php echo $MaestroLink_Equity_Plus_IDR_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="MaestroLink_Fixed_Income_Plus_USD">MaestroLink Fixed Income Plus (USD)</label></td>
				<td>Bid: <input type="text" name="MaestroLink_Fixed_Income_Plus_USD_BID" id="MaestroLink_Fixed_Income_Plus_USD_BID" value="<?php echo $MaestroLink_Fixed_Income_Plus_USD_BID; ?>" /></td>
				<td>Offer: <input type="text" name="MaestroLink_Fixed_Income_Plus_USD_OFFER" id="MaestroLink_Fixed_Income_Plus_USD_OFFER" value="<?php echo $MaestroLink_Fixed_Income_Plus_USD_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="MaestroLink_Balanced_IDR">MaestroLink Balanced (IDR)</label></td>
				<td>Bid: <input type="text" name="MaestroLink_Balanced_IDR_BID" id="MaestroLink_Balanced_IDR_BID" value="<?php echo $MaestroLink_Balanced_IDR_BID; ?>" /></td>
				<td>Offer: <input type="text" name="MaestroLink_Balanced_IDR_OFFER" id="MaestroLink_Balanced_IDR_OFFER" value="<?php echo $MaestroLink_Balanced_IDR_OFFER; ?>" /></td>
			</tr>
			
			<tr>
				<td><label for="Maestro_Equity_Syariah_IDR">Maestro Equity Syariah (IDR)</label></td>
				<td>Bid: <input type="text" name="Maestro_Equity_Syariah_IDR_BID" id="Maestro_Equity_Syariah_IDR_BID" value="<?php echo $Maestro_Equity_Syariah_IDR_BID; ?>" /></td>
				<td>Offer: <input type="text" name="Maestro_Equity_Syariah_IDR_OFFER" id="Maestro_Equity_Syariah_IDR_OFFER" value="<?php echo $Maestro_Equity_Syariah_IDR_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="Maestro_Balance_Syariah_IDR">Maestro Balance Syariah (IDR)</label></td>
				<td>Bid: <input type="text" name="Maestro_Balance_Syariah_IDR_BID" id="Maestro_Balance_Syariah_IDR_BID" value="<?php echo $Maestro_Balance_Syariah_IDR_BID; ?>" /></td>
				<td>Offer: <input type="text" name="Maestro_Balance_Syariah_IDR_OFFER" id="Maestro_Balance_Syariah_IDR_OFFER" value="<?php echo $Maestro_Balance_Syariah_IDR_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="MaestroLink_Dynamic_IDR">MaestroLink Dynamic (IDR)</label></td>
				<td>Bid: <input type="text" name="MaestroLink_Dynamic_IDR_BID" id="MaestroLink_Dynamic_IDR_BID" value="<?php echo $MaestroLink_Dynamic_IDR_BID; ?>" /></td>
				<td>Offer: <input type="text" name="MaestroLink_Dynamic_IDR_OFFER" id="MaestroLink_Dynamic_IDR_OFFER" value="<?php echo $MaestroLink_Dynamic_IDR_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="MaestroLink_Aggresive_Equity_IDR">MaestroLink Aggresive Equity (IDR)</label></td>
				<td>Bid: <input type="text" name="MaestroLink_Aggresive_Equity_IDR_BID" id="MaestroLink_Aggresive_Equity_IDR_BID" value="<?php echo $MaestroLink_Aggresive_Equity_IDR_BID; ?>" /></td>
				<td>Offer: <input type="text" name="MaestroLink_Aggresive_Equity_IDR_OFFER" id="MaestroLink_Aggresive_Equity_IDR_OFFER" value="<?php echo $MaestroLink_Aggresive_Equity_IDR_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="AFI_Dynamic_Money_IDR">AFI Dynamic Money (IDR)</label></td>
				<td>Bid: <input type="text" name="AFI_Dynamic_Money_IDR_BID" id="AFI_Dynamic_Money_IDR_BID" value="<?php echo $AFI_Dynamic_Money_IDR_BID; ?>" /></td>
				<td>Offer: <input type="text" name="AFI_Dynamic_Money_IDR_OFFER" id="AFI_Dynamic_Money_IDR_OFFER" value="<?php echo $AFI_Dynamic_Money_IDR_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="AFI_Progressive_Money_IDR">AFI Progressive Money (IDR)</label></td>
				<td>Bid: <input type="text" name="AFI_Progressive_Money_IDR_BID" id="AFI_Progressive_Money_IDR_BID" value="<?php echo $AFI_Progressive_Money_IDR_BID; ?>" /></td>
				<td>Offer: <input type="text" name="AFI_Progressive_Money_IDR_OFFER" id="AFI_Progressive_Money_IDR_OFFER" value="<?php echo $AFI_Progressive_Money_IDR_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="AFI_Secure_Money_IDR">AFI Secure Money (IDR)</label></td>
				<td>Bid: <input type="text" name="AFI_Secure_Money_IDR_BID" id="AFI_Secure_Money_IDR_BID" value="<?php echo $AFI_Secure_Money_IDR_BID; ?>" /></td>
				<td>Offer: <input type="text" name="AFI_Secure_Money_IDR_OFFER" id="AFI_Secure_Money_IDR_OFFER" value="<?php echo $AFI_Secure_Money_IDR_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="Secure_Money_USD">Secure Money (USD)</label></td>
				<td>Bid: <input type="text" name="Secure_Money_USD_BID" id="Secure_Money_USD_BID" value="<?php echo $Secure_Money_USD_BID; ?>" /></td>
				<td>Offer: <input type="text" name="Secure_Money_USD_OFFER" id="Secure_Money_USD_OFFER" value="<?php echo $Secure_Money_USD_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="Money_Market_IDR">Money Market (IDR)</label></td>
				<td>Bid: <input type="text" name="Money_Market_IDR_BID" id="Money_Market_IDR_BID" value="<?php echo $Money_Market_IDR_BID; ?>" /></td>
				<td>Offer: <input type="text" name="Money_Market_IDR_OFFER" id="Money_Market_IDR_OFFER" value="<?php echo $Money_Market_IDR_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="Syariah_Dynamic">Syariah Dynamic (IDR)</label></td>
				<td>Bid: <input type="text" name="Syariah_Dynamic_BID" id="Syariah_Dynamic_BID" value="<?php echo $Syariah_Dynamic_BID; ?>" /></td>
				<td>Offer: <input type="text" name="Syariah_Dynamic_OFFER" id="Syariah_Dynamic_OFFER" value="<?php echo $Syariah_Dynamic_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="Syariah_Progressive">Syariah Progressive (IDR)</label></td>
				<td>Bid: <input type="text" name="Syariah_Progressive_BID" id="Syariah_Progressive_BID" value="<?php echo $Syariah_Progressive_BID; ?>" /></td>
				<td>Offer: <input type="text" name="Syariah_Progressive_OFFER" id="Syariah_Progressive_OFFER" value="<?php echo $Syariah_Progressive_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="Maestro_Progressive_Equity_Syariah">Maestro Progressive Equity Syariah (IDR)</label></td>
				<td>Bid: <input type="text" name="Maestro_Progressive_Equity_Syariah_BID" id="Maestro_Progressive_Equity_Syariah_BID" value="<?php echo $Maestro_Progressive_Equity_Syariah_BID; ?>" /></td>
				<td>Offer: <input type="text" name="Maestro_Progressive_Equity_Syariah_OFFER" id="Maestro_Progressive_Equity_Syariah_OFFER" value="<?php echo $Maestro_Progressive_Equity_Syariah_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="Maestrolink_Maxi_Advantage">Maestro link Maxi Advantage(IDR)</label></td>
				<td>Bid: <input type="text" name="Maestrolink_Maxi_Advantage_BID" id="Maestrolink_Maxi_Advantage_BID" value="<?php echo $Maestrolink_Maxi_Advantage_BID; ?>" /></td>
				<td>Offer: <input type="text" name="Maestrolink_Maxi_Advantage_OFFER" id="Maestrolink_Maxi_Advantage_OFFER" value="<?php echo $Maestrolink_Maxi_Advantage_OFFER; ?>" /></td>
				<td>GP: <input type="text" name="Maestrolink_Maxi_Advantage_GP" id="Maestrolink_Maxi_Advantage_GP" value="<?php echo $Maestrolink_Maxi_Advantage_GP; ?>" /></td>
			</tr>
			
			<tr>
				<td><label for="MaestroBerimbang">Maestro Berimbang (IDR)</label></td>
				<td>Bid: <input type="text" name="MaestroBerimbang_BID" id="MaestroBerimbang_BID" value="<?php echo $MaestroBerimbang_BID; ?>" /></td>
				<td>Offer: <input type="text" name="MaestroBerimbang_OFFER" id="MaestroBerimbang_OFFER" value="<?php echo $MaestroBerimbang_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="AXA_CitraDinamis">AXACitraDinamis (IDR)</label></td>
				<td>Bid: <input type="text" name="AXA_CitraDinamis_BID" id="AXA_CitraDinamis_BID" value="<?php echo $AXA_CitraDinamis_BID; ?>" /></td>
				<td>Offer: <input type="text" name="AXA_CitraDinamis_OFFER" id="AXA_CitraDinamis_OFFER" value="<?php echo $AXA_CitraDinamis_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="CitraGold">Citra Gold (IDR)</label></td>
				<td>Bid: <input type="text" name="CitraGold_BID" id="CitraGold_BID" value="<?php echo $CitraGold_BID; ?>" /></td>
				<td>Offer: <input type="text" name="CitraGold_OFFER" id="CitraGold_OFFER" value="<?php echo $CitraGold_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="AXA_MaestroDollar">Maestro Dollar (IDR)</label></td>
				<td>Bid: <input type="text" name="AXA_MaestroDollar_BID" id="AXA_MaestroDollar_BID" value="<?php echo $AXA_MaestroDollar_BID; ?>" /></td>
				<td>Offer: <input type="text" name="AXA_MaestroDollar_OFFER" id="AXA_MaestroDollar_OFFER" value="<?php echo $AXA_MaestroDollar_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="AXA_MaestroObligasi_Plus">AXA Maestro Obligasi Plus (IDR)</label></td>
				<td>Bid: <input type="text" name="AXA_MaestroObligasi_Plus_BID" id="AXA_MaestroObligasi_Plus_BID" value="<?php echo $AXA_MaestroObligasi_Plus_BID; ?>" /></td>
				<td>Offer: <input type="text" name="AXA_MaestroObligasi_Plus_OFFER" id="AXA_MaestroObligasi_Plus_OFFER" value="<?php echo $AXA_MaestroObligasi_Plus_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="AXA_MaestroSaham">AXA Maestro Saham (IDR)</label></td>
				<td>Bid: <input type="text" name="AXA_MaestroSaham_BID" id="AXA_MaestroSaham_BID" value="<?php echo $AXA_MaestroSaham_BID; ?>" /></td>
				<td>Offer: <input type="text" name="AXA_MaestroSaham_OFFER" id="AXA_MaestroSaham_OFFER" value="<?php echo $AXA_MaestroSaham_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="ALI_Dynamic">ALI Dynamic (IDR)</label></td>
				<td>Bid: <input type="text" name="ALI_Dynamic_BID" id="ALI_Dynamic_BID" value="<?php echo $ALI_Dynamic_BID; ?>" /></td>
				<td>Offer: <input type="text" name="ALI_Dynamic_OFFER" id="ALI_Dynamic_OFFER" value="<?php echo $ALI_Dynamic_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="ALI_Progressive">ALI Progressive (IDR)</label></td>
				<td>Bid: <input type="text" name="ALI_Progressive_BID" id="ALI_Progressive_BID" value="<?php echo $ALI_Progressive_BID; ?>" /></td>
				<td>Offer: <input type="text" name="ALI_Progressive_OFFER" id="ALI_Progressive_OFFER" value="<?php echo $ALI_Progressive_OFFER; ?>" /></td>
			</tr>
			<tr>
				<td><label for="ALI_Secure">ALI Secure (IDR)</label></td>
				<td>Bid: <input type="text" name="ALI_Secure_BID" id="ALI_Secure_BID" value="<?php echo $ALI_Secure_BID; ?>" /></td>
				<td>Offer: <input type="text" name="ALI_Secure_OFFER" id="ALI_Secure_OFFER" value="<?php echo $ALI_Secure_OFFER; ?>" /></td>
			</tr>
			
			<tr>
				<td>&nbsp;</td>
				<td><input type="submit" name="submit" value="Submit" /></td>
			</tr>
			
		</table>
	</form>
	

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script type="text/javascript" >

		var desiredValue = <?php echo ($month) ? $month : date('m'); ?>;
		var el = document.getElementById("mm");
		for(var i=0; i<el.options.length; i++) {
		  if ( el.options[i].value == desiredValue ) {
		    el.selectedIndex = i;

		    break;
		  }
		}

		jQuery('input[type=text]').change(function() {
		    jQuery(this).val(jQuery(this).val().replace(/,/g,''));
		});		

	</script>
	
</div>
