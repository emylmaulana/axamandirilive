<?php 
class AXA_Ulip{
	
	protected $ulip = "all";
	protected $product;
	
	public function get_data($data,$product){
		global $wpdb;
	
		if( $data = 'all' && empty($product)) :
			$query = $wpdb->get_results("SELECT * FROM wp_ulip_rate ORDER BY Date DESC LIMIT 2",ARRAY_A);
		elseif( $data = 'product' && !empty($product) && !isset($_GET['datequery']) ) :
			$unit = $product;
			$product = $product.'_BID,'.$product.'_OFFER';
			$query = $wpdb->get_results("SELECT Date,$product FROM wp_ulip_rate where ".$unit."_BID != 0.0000 ORDER BY Date DESC LIMIT 10");
		elseif( $data = 'product' && !empty($product) && isset($_GET['datequery']) && !empty($_GET['datequery']) ) :
			$unit = $product;
			$product = $product.'_BID,'.$product.'_OFFER';
			$startDate = $_GET['startDate'];
			$endDate = $_GET['endDate'];
			$endDateTime = strtotime($endDate . ' + 1 day');
			$endDateString = date('Y-m-d',strtotime($endDate . ' + 1 day'));
			$query = $wpdb->get_results("SELECT Date, $product FROM wp_ulip_rate WHERE ".$unit."_BID != 0.0000 AND Date BETWEEN '$startDate' AND '$endDateString' ORDER BY Date DESC");
		endif;
		
		return $query;
	}
	
	function construct_data_all($query){
			
		//Set Product Title
		$newstructure[0][] = __('Jenis Produk','axa-financial');
		$newstructure[1][] = 'MaestroLink Fixed Income Plus (IDR)';
		$newstructure[2][] = 'MaestroLink Cash Plus (IDR)';
		$newstructure[3][] = 'MaestroLink Equity Plus (IDR)';
		$newstructure[4][] = 'MaestroLink Fixed Income Plus (USD)';		
        $newstructure[5][] = 'MaestroLink Balanced (IDR)';
        $newstructure[6][] = 'Maestro Equity Syariah (IDR)';
        $newstructure[7][] = 'Maestro Balance Syariah (IDR)';
        $newstructure[8][] = 'MaestroLink Dynamic (IDR)';
        $newstructure[9][] = 'MaestroLink Aggresive Equity (IDR)';
        $newstructure[10][] = 'AFI Dynamic Money (IDR)';
        $newstructure[11][] = 'AFI Progressive Money (IDR)'; 
        $newstructure[12][] = 'AFI Secure Money (IDR)';
        $newstructure[13][] = 'Secure Money (USD)';
        $newstructure[14][] = 'Money Market (IDR)';  
        $newstructure[15][] = 'Syariah Dynamic (IDR)';
        $newstructure[16][] = 'Syariah Progressive (IDR)';
		$newstructure[17][] = 'Maestro Progressive Equity Syariah(IDR)';
		$newstructure[18][] = 'Maestro Link Maxi Advantage(IDR)';
		$newstructure[19][] = 'Maestro Berimbang(IDR)';
		$newstructure[20][] = 'AXA Citra Dinamis(IDR)';
		$newstructure[21][] = 'Citra Gold(IDR)';
		$newstructure[22][] = 'AXA Maestro Dollar(USD)';
		$newstructure[23][] = 'AXA Maestro Obligasi Plus(IDR)';
		$newstructure[24][] = 'AXA Maestro Saham(IDR)';
		$newstructure[25][] = 'ALI Dynamic(IDR)';
		$newstructure[26][] = 'ALI Progressive(IDR)';
		$newstructure[27][] = 'ALI Secure(IDR)';

		foreach ($query as $key=>$value) {
			
			
			$newstructure[0][] = $value['Date'];
			$newstructure[1][] = $value['MaestroLink_Fixed_Income_Plus_IDR_BID'];
			$newstructure[2][] = $value['MaestroLink_Cash_Plus_IDR_BID'];
			$newstructure[3][] = $value['MaestroLink_Equity_Plus_IDR_BID'];
			$newstructure[4][] = $value['MaestroLink_Fixed_Income_Plus_USD_BID'];	
	        $newstructure[5][] = $value['MaestroLink_Balanced_IDR_BID'];
	        $newstructure[6][] = $value['Maestro_Equity_Syariah_IDR_BID'];
	        $newstructure[7][] = $value['Maestro_Balance_Syariah_IDR_BID'];
	        $newstructure[8][] = $value['MaestroLink_Dynamic_IDR_BID'];
	        $newstructure[9][] = $value['MaestroLink_Aggresive_Equity_IDR_BID'];
	        $newstructure[10][] = $value['AFI_Dynamic_Money_IDR_BID'];
	        $newstructure[11][] = $value['AFI_Progressive_Money_IDR_BID']; 
	        $newstructure[12][] = $value['AFI_Secure_Money_IDR_BID'];
	        $newstructure[13][] = $value['Secure_Money_USD_BID'];
	        $newstructure[14][] = $value['Money_Market_IDR_BID'];  
	        $newstructure[15][] = $value['Syariah_Dynamic_BID'];
	        $newstructure[16][] = $value['Syariah_Progressive_BID'];
	        $newstructure[17][] = $value['Maestro_Progressive_Equity_Syariah_BID'];
			$newstructure[18][] = $value['Maestrolink_Maxi_Advantage_BID'];			
	        $newstructure[19][] = $value['MaestroBerimbang_BID'];
			$newstructure[20][] = $value['AXA_CitraDinamis_BID'];
			$newstructure[21][] = $value['CitraGold_BID'];
			$newstructure[22][] = $value['AXA_MaestroDollar_BID'];
			$newstructure[23][] = $value['AXA_MaestroObligasi_Plus_BID'];
			$newstructure[24][] = $value['AXA_MaestroSaham_BID'];
			$newstructure[25][] = $value['ALI_Dynamic_BID'];
			$newstructure[26][] = $value['ALI_Progressive_BID'];
			$newstructure[27][] = $value['ALI_Secure_BID'];
			
			}
		$count= count($newstructure);
		for($i=1;$i<$count;$i++){
			$newstructure[$i][] = $newstructure[$i][1]-$newstructure[$i][2];	
		}

		$newstructure[1][] = 'maestrolink-fixed-income-plus-idr';
		$newstructure[2][] = 'maestrolink-cash-plus-idr';
		$newstructure[3][] = 'maestrolink-equity-plus-idr';
		$newstructure[4][] = 'maestrolink-fixed-income-plus-usd';
        $newstructure[5][] = 'maestrolink-balanced-idr';
        
        $newstructure[6][] = 'maestro-equity-syariah-idr';
        $newstructure[7][] = 'maestro-balance-syariah-idr';
        $newstructure[8][] = 'maestrolink-dynamic-idr';
        $newstructure[9][] = 'maestrolink-aggresive-equity-idr';
        $newstructure[10][] = 'afi-dynamic-money-idr';
        $newstructure[11][] = 'afi-progressive-money-idr'; 
        $newstructure[12][] = 'afi-secure-money-idr';
        $newstructure[13][] = 'secure-money-usd';
        $newstructure[14][] = 'money-market-idr';  
        $newstructure[15][] = 'syariah-dynamic';
        $newstructure[16][] = 'syariah-progressive';
    	$newstructure[17][] = 'maestro-progressive-equity-syariah';
		$newstructure[18][] = 'maestrolink-maxi-advantage';		
        $newstructure[19][] = 'maestro-berimbang';
		$newstructure[20][] = 'axa-citra-dinamis';
		$newstructure[21][] = 'citra-gold';
		$newstructure[22][] = 'axa-maestro-dollar';
		$newstructure[23][] = 'axa-maestro-obligasi-plus';
		$newstructure[24][] = 'axa-maestro-saham';
		$newstructure[25][] = 'ali-dynamic';
		$newstructure[26][] = 'ali-progressive';
		$newstructure[27][] = 'ali-secure';

		return $newstructure;
	
	}

	function construct_data_product($query,$product){
			
		//setlocale(LC_ALL, 'id_ID', 'IND', 'Indonesian', 'Indonesia');
		//Note: Offer is hidden pending further notice remove HTML comment <!-- --> when necessary
		if($query) :
			$productBid = $product.'_BID';
			$productOffer = $product.'_OFFER';
			$output = "<table width='420px' id='unit-table' cellspacing='0'>";
			$output .= "<thead>
				<tr>
					<th>".__('Tanggal','axa-financial')."</th>
					<th>".__('Harga Bid','axa-financial')."</th>
					<!--<th>".__('Harga Offer','axa-financial')."</th>-->
				</tr>
				</thead>
				<tbody>";

					foreach($query as $value){
						$output .= "<tr><td>".strftime('%e/%b/%Y',strtotime($value->Date))."</td>";	
						$output .= "<td>".number_format($value->$productBid,'2','.',',')."</td>";	
						$output .= "<!--<td>".number_format($value->$productOffer,'2','.',',')."</td></tr>-->";	
					}
			$output .= "</tbody></table>";
			
			//$output .= "<div class='news-pagination'>".wp_pagenavi(array( 'query' => $query ))."</div>";
		else :
			$output = __('Data tidak tersedia untuk tanggal ini','axa-financial');
		endif;
		echo $output;
		}

		function set_table($add){
		
		setlocale(LC_ALL, 'id_ID', 'IND', 'Indonesian', 'Indonesia');
		$newstructure = $add;
			
		$output  = "<table id='unit-table' cellspacing='0' width='100%'>";
		$output .= 	"<thead>
						<tr>
							<th>".$newstructure[0][0]."</th>
							<th>".strftime( '%e/%b/%Y',strtotime($newstructure[0][1]) )."</th>
							<th>".strftime( '%e/%b/%Y',strtotime($newstructure[0][2]) )."</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>";
		unset($newstructure[0]);
		
		sort($newstructure);
		
		foreach($newstructure as $value){
			$output .= "<tr>";
			$output .= "<td class=\"product-name\">".$value[0]."</td>"."<td>".number_format($value[1],'4','.',',')."</td>"."<td>".number_format($value[2],'4','.',',')."</td>";
			
			if ( $value[3] < 0 ){ $class = "min"; } else { $class = "up"; }
			
			$output .= "<td class=\"allresult ".$class."\"><span></span>".number_format(abs($value[3]),4)."</td>"."<td><a href=\"".$value[4]."\" class=\"btn-medium\">".__('selengkapnya','axa-financial')."</a></td>";
			$output .= "</tr>";
		}
		
		$output .= "</tbody></table>";
		
		return $output;
	}
		
	function set_widget($add){
		//setlocale(LC_ALL, 'id_ID', 'IND', 'Indonesian', 'Indonesia');
		$newstructure = $add;

		$older_date = strftime('%e/%b/%Y',strtotime($newstructure[0][1]));
		$latest_date = strftime('%e/%b/%Y',strtotime($newstructure[0][2]));
		
		$output = "<div class='slideshow-unit owl-carousel'>";
		unset($newstructure[0]);
		sort($newstructure);
		
		foreach($newstructure as $value){
			if($value[1]!="0.0000"||$value[2]!="0.0000"){
		$output .= "<div class='slide-unit'>";
		$output .= "<ul class='unit-item'>";		
		$output .= "<li><h4 class='product-title'>".$value[0]."</h4></li>";
		$output .= "<li><div class='unit'>";
		$output .= "<span class='date'>".$older_date."</span>";
		$output .= "<span class='result'>".number_format($value[1],'4','.',',')."</span>";
		$output .= "</div></li>";
		$output .= "<li><div class='unit'>";
		$output .= "<span class='date'>".$latest_date."</span>";
		$output .= "<span class='result'>".number_format($value[2],'4','.',',')."</span>";
		$output .= "</div></li>";			
		
		if ( $value[3] < 0 ){ $class = "min"; } else { $class = "up"; }
		$output .= "<li><div class='allresult ".$class."'>".number_format(abs($value[3]),'4','.',',')."</div></li>";				
		$output .= "</ul>";		
		$output .= "</div>";				
		}

		}
		$output .= "</div>";
		return $output;		
	}

}

//Display Ulip
function display_ulip($atts){
	$datequery = $_GET['datequery'];
	//extract ulip shortcode attribute	
	extract(shortcode_atts(array(
		'ulip' => 'all',
		'product' => '',
		),$atts));
		
	$Ulip = new AXA_Ulip;

	$getdata = $Ulip->get_data($ulip,$product);	

	/* Editan Said
	===============================================*/
	if( $ulip = 'all' && empty($product) ) : 
			$add = $Ulip->construct_data_all($getdata);
			return $Ulip->set_table($add);
	elseif( $ulip = 'product' && !empty($product) && !isset($datequery)) : $add = $Ulip->construct_data_product($getdata,$product);
	elseif( $ulip = 'product' && !empty($product) && isset($datequery) ) : $add = $Ulip->construct_data_product($getdata,$product);

	endif;
	//echo json_encode($getdata);
}

function display_ulip_widget(){
	$Ulip = new AXA_Ulip;
	$getdata = $Ulip->get_data('all','');
	$add = $Ulip->construct_data_all($getdata);
	return $Ulip->set_widget($add);
}

add_shortcode('hargaunitharian','display_ulip');
add_shortcode('widgetunitharian','display_ulip_widget');
?>