<?php 

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class TotalClaimListTable extends WP_List_Table {

function get_totalclaim_data(){
global $wpdb;

	$example_data = $wpdb->get_results('SELECT * FROM wp_total_claim ORDER BY last_update ASC', ARRAY_A);
	return $example_data;
}

function __construct(){
global $status, $page;

    parent::__construct( array(
        'singular'  => __( 'claim', 'xratelisttable' ),     //singular name of the listed records
        'plural'    => __( 'claims', 'xratelisttable' ),   //plural name of the listed records
        'ajax'      => false        //does this table support ajax?
	) );
	
	}
	
function column_default( $item, $column_name ) {
	switch( $column_name ) { 
        case 'idr':
			echo number_format($item[$column_name],2,',','.');
			break;
        case 'usd':
			echo number_format($item[$column_name],2,',','.');
			break;
		case 'last_update':
			echo date("F d, Y",strtotime($item[$column_name]));			
			break;
		case 'edit':
			echo '<a href="admin.php?page=totalclaim-add&id='.$item['totalclaim_id'].'&action=edit">edit</a>';
		}
	}
  
function get_columns(){
        $columns = array(
            'last_update'      => __( 'Last Update', 'xratelisttable' ),
            'idr'    => __( 'IDR', 'xratelisttable' ),
            'usd'      => __( 'USD', 'xratelisttable' ),
            'edit'      => __( 'Edit', 'xratelisttable' )
        );
         return $columns;
    } 

function get_sortable_columns(){
	$sortable_columns = array(
		'last_update' => array('last_update',true)
	);
	
	return $sortable_columns;
}
 
function prepare_items() {
	
	$per_page = 20;
	
	$columns  = $this->get_columns();
	$hidden   = array();
	$sortable = $this->get_sortable_columns();
	$this->_column_headers = array( $columns, $hidden, $sortable );
	
	$data = $this->get_totalclaim_data();

        function usort_reorder($a,$b){
            $orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'last_update'; //If no sort, default to title
            $order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'asc'; //If no order, default to asc
            $result = strcmp($a[$orderby], $b[$orderby]); //Determine sort order
            return ($order==='asc') ? $result : -$result; //Send final sort direction to usort
        }
        usort($data, 'usort_reorder');
		
	$current_page = $this->get_pagenum();
    $total_items = count($data);
    $data = array_slice($data,(($current_page-1)*$per_page),$per_page);
    $this->items = $data;
	
        $this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items/$per_page)   //WE have to calculate the total number of pages
        ) );
	} 

}

function totalclaim_admin_page_add(){
	require_once('edit-totalclaim.php');
}

function totalclaim_render_page(){
  $myListTable = new TotalClaimListTable();
  echo '</pre><div class="wrap"><h2>Today\'s Total Claim</h2>'; 
  $myListTable->prepare_items(); 
  $myListTable->display(); 
  echo '</div>'; 
}

?>