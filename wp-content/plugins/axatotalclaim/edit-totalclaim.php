<?php
// kill the page when someone have unsufficient privilege
if( !current_user_can('manage_options') ) wp_die(__('You do not have sufficient permissions to import content in this site.'));

require_once('totalclaim.php');

$title = __('Total Claim');


if(!isset($_GET['action'])){
	$action = "add";
} else {
	$action = $_GET['action'];
}


if( $_GET['action']=="edit" && isset($_GET['id'])){
	
	global $wpdb;
	$totalclaimid = $_GET['id']; 
	$query = $wpdb->get_row("SELECT * FROM wp_total_claim WHERE totalclaim_id=$totalclaimid");
	
	$totalclaimidr = $query->idr;
	$totalclaimusd = $query->usd;
	$timestamp = strtotime($query->last_update);
	$day = date('d', $timestamp);
	$month = date('m', $timestamp);
	$year = date('Y', $timestamp);	
}

?>

<div class="wrap">
	<?php screen_icon($screen); ?>
	<h2><?php echo $title; ?></h2>
	<p><?php _e('This page is for adding total claim'); ?></p>
	

	<?php 
		if(count($infos) > 0 ){
	?>
	<div class="infos">
	<?php			
			foreach ($infos as $value) {
				echo $value;
			}
	?>
	</div>
	<?php		
		}
	?>
	</div>
	
	<form name="totalclaim-edit" id="totalclaim-edit" method="post" action="" >
	<input type="hidden" name="action" value="<?php echo $action ?>"/>
	<input type="hidden" name="totalclaimid" value="<?php echo $totalclaimid ?>"/>
	<?php wp_nonce_field('totalclaim-add','totalclaim_nonce_field'); ?>
		<table>
			<tr>
				<td colspan="1">Date</td>
				<td>
					
					<div class="timestamp-wrap">
						<input type="text" id="jj" name="jj" value="<?php echo ($day) ? $day : date("d"); ?>" size="2" maxlength="2" tabindex="4" autocomplete="off" style="text-align: center;">&nbsp;&nbsp;/&nbsp;
						<select id="mm" name="mm" tabindex="4">
							<option value="01">Jan</option>
							<option value="02">Feb</option>
							<option value="03">Mar</option>
							<option value="04">Apr</option>
							<option value="05">May</option>
							<option value="06">Jun</option>
							<option value="07">Jul</option>
							<option value="08">Aug</option>
							<option value="09">Sep</option>
							<option value="10">Oct</option>
							<option value="11">Nov</option>
							<option value="12">Dec</option>
						</select>
						
						&nbsp;/&nbsp;&nbsp; <input type="text" id="aa" name="aa" value="<?php echo ($year) ? $year : date("Y"); ?>" size="4" maxlength="4" tabindex="4" autocomplete="off"  style="text-align: center;">
					</div>					

				</td>
			</tr>
			<tr>
				<td><label for="totalclaimIDR">IDR</label></td>
				<td><input type="text" name="totalclaimIDR" id="totalclaimIDR" value="<?php echo $totalclaimidr; ?>" /></td>
			</tr>
			<tr>
				<td><label for="totalclaimUSD">Total Claim USD</label></td>
				<td><input type="text" name="totalclaimUSD" id="totalclaimUSD" value="<?php echo $totalclaimusd; ?>" /></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><input type="submit" name="submit" /></td>
			</tr>
		</table>
	</form>

</div>

	<script type="text/javascript" >

		var desiredValue = <?php echo ($month) ? $month : date('m')-1; ?>;
		var el = document.getElementById("mm");
		for(var i=0; i<el.options.length; i++) {
		  if ( el.options[i].value == desiredValue ) {
		    el.selectedIndex = i;

		    break;
		  }
		}

		jQuery('input[type=text]').change(function() {
		    jQuery(this).val(jQuery(this).val().replace(/,/g,''));
		});				

	</script>
<?php
?>