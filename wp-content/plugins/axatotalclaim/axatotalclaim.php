<?php
/*
Plugin Name: AXA Financial Total Claim
Plugin URI: 
Description: Total Claim Plugin for AXA Financial Website
Version: 0.1
Author: Definite Studio
Author URI: http://www.definitelyworks.com
License: -
*/
?>
<?php

//Initialize Admin Menu
add_action('admin_menu','register_totalclaim_admin_menu');

include_once dirname(__FILE__).'/adminpage.php';
include_once dirname(__FILE__).'/widget.php';

function register_totalclaim_admin_menu(){
	add_menu_page( 'Total Claim', 'Total Claim', 'manage_options', 'total-claim', 'totalclaim_render_page', '', 21 );
	$page = add_submenu_page('total-claim', 'Add Today\'s Total Claim', 'Add Total Claim', 'manage_options', 'totalclaim-add','totalclaim_admin_page_add');

	add_action('admin_print_scripts-'.$page,'totalclaim_scripts');
}

function totalclaim_scripts(){
	wp_enqueue_script('jquery-validate',plugins_url('/js/jquery.validate.min.js',__FILE__));
	wp_enqueue_script('init-validation',plugins_url('/js/init.validation.js',__FILE__));
	wp_enqueue_style('totalclaim-style', plugins_url('/css/style.css',__FILE__));
}

//settings

?>