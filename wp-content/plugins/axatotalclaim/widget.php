<?php

class AXA_TotalClaim_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct(
	 		'totalclaim_widget', // Base ID
			'AXA_TotalClaim_Widget', // Name
			array( 'description' => __( 'Widget to display Total Claim', 'text_domain' ), ) // Args
		);
	}

	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'text_domain' );
		}
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );

		return $instance;
	}

	public function widget( $args, $instance ) {
		global $wpdb;
		extract( $args );

		$query = $wpdb->get_row("SELECT * FROM wp_total_claim ORDER BY last_update DESC LIMIT 1");
		
		$timestamp = strtotime($query->last_update);
		
		setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');
		
		?>
		<div class="bottom-widget clearfix last-child">
			<div class="left section">
				<h5><?php echo __('Total Klaim','axa-financial') ?></h5>
				<p class="date"><?php echo strftime('%h %Y',$timestamp); ?></p>
			</div>
			
			<div class="right section">
				<table cellspacing="0;">
					<tr>
						<td><small>IDR</small></td>
						<td><span style="text-align:right"><?php echo number_format($query->idr,'2','.',','); ?></span></td>
					</tr>
					<tr>
						<td><small>USD</small></td>
						<td><span style="text-align:right"><?php echo number_format($query->usd,'2','.',','); ?></span></td>
					</tr>
				</table>
			</div>
		</div><!--end bottom widget-->
		<?php
	}

}

add_action( 'widgets_init', create_function( '', 'register_widget( "AXA_TotalClaim_Widget" );' ) );

?>