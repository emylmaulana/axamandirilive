jQuery(document).ready(function($){
	//form validation
	$('#totalclaim-edit').validate({
		  rules: {
		    totalclaimIDR : {
		      required: true,
		      number: true
		    },
		    totalclaimUSD : {
		      required: true,
		      number: true
		    }
		  }		
	});
})
