<?php 

global $wpdb;

$infos = array();
	
if($_POST['action']=='add' && wp_verify_nonce($_POST['totalclaim_nonce_field'],'totalclaim-add') ){
		
	$totalclaimIDR = $_POST['totalclaimIDR'];
	$totalclaimUSD = $_POST['totalclaimUSD'];	
	$totalclaimDate = date('Y-m-d H:i:s', mktime(0,0,0, $_POST['mm'],$_POST['jj'],$_POST['aa']));
	
	if(check_totalclaim_currentdate()){
		//echo "INSERT INTO wp_total_claim(idr,usd,last_update) VALUES($totalclaimIDR,$totalclaimUSD,NOW())";
		$query = $wpdb->query("INSERT INTO wp_total_claim(idr,usd,last_update) VALUES('$totalclaimIDR','$totalclaimUSD','$totalclaimDate')");
						
		if($query) : $infos[] .= 'total claim is successfully added';
		else : $infos[] .= 'something went wrong, please try again';
		endif;
	} else {
		$infos[] .= 'There\'s already a total for that date, please use edit to change total claim';
	}
					
} 


if($_POST['action']=="edit" && wp_verify_nonce($_POST['totalclaim_nonce_field'],'totalclaim-add')){
	$totalclaimid = $_POST['totalclaimid'];
	$totalclaimIDR = $_POST['totalclaimIDR'];
	$totalclaimUSD = $_POST['totalclaimUSD'];		

	$query = $wpdb->update('wp_total_claim',array(
				'idr' => $totalclaimIDR,
				'usd' => $totalclaimUSD,
				),array(
				'totalclaim_id' => $totalclaimid
				)
			);
		
	if( $query >= 0 ) : $infos[] .= 'total exchange claim is successfully modified';
	else : $infos[] .= 'something went wrong, please try again';
	endif;
}

function check_totalclaim_currentdate(){
	global $wpdb;		
	$today = $_POST['aa'].'-'.$_POST['mm'].'-'.$_POST['jj'];
	$rowdate = $wpdb->get_var("SELECT * FROM wp_total_claim WHERE last_update LIKE '%$today%'");

	if(empty($rowdate)){
		return true;
	} else {
		return false;
	}
}

?>