<?php
// kill the page when someone have unsufficient privilege
if( !current_user_can('manage_options') ) wp_die(__('You do not have sufficient permissions to import content in this site.'));

//require_once('ulip.php');

function admin_ulip_list () {
?>
<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/sinetiks-schools/style-admin.css" rel="stylesheet" />
<div class="wrap">
<h2>Product List ULIP</h2>
<a href="<?php echo admin_url('admin.php?page=add-ulip-product'); ?>">Add New</a>
<?php
global $wpdb;
$rows = $wpdb->get_results("show columns from wp_ulip_rate where Field like '%BID%' and Field !='id' and Field !='Date'");
// var_dump($rows);
echo "<table class='wp-list-table widefat'>";
echo "<tr><th>Name Product</th><th>Type</th><th>&nbsp;</th></tr>";
foreach ($rows as $row ){
	$rpl = array("_BID","_");
	echo "<tr>";
	echo "<td>".str_replace($rpl, ' ', $row->Field)."</td>";
	echo "<td>$row->Type</td>";	
	echo "<td><a href='".admin_url('admin.php?page=ulip-update&Field='.$row->Field)."'>Update</a></td>";
	echo "</tr>";}
echo "</table>";
?>
</div>
<?php
}

admin_ulip_list();