<?php
if( !current_user_can('manage_options') ) wp_die(__('You do not have sufficient permissions to import content in this site.'));

function admin_ulip_update () {
global $wpdb;
$Field = $_GET["Field"];
$name=$_POST["name"];
$old_name = $_POST['old_name'];
//update
if(isset($_POST['update'])){	
	
	$wpdb->query("ALTER TABLE  `wp_ulip_rate` CHANGE  ".$old_name."_BID ".str_replace(' ', '_', $name)."_BID DECIMAL( 19, 4 ) NULL DEFAULT  '0.0000';");
	$wpdb->query("ALTER TABLE  `wp_ulip_rate` CHANGE  ".$old_name."_OFFER ".str_replace(' ', '_', $name)."_OFFER DECIMAL( 19, 4 ) NULL DEFAULT  '0.0000';");
}
//delete
else if(isset($_POST['delete'])){	
	$wpdb->query("ALTER TABLE `wp_ulip_rate` DROP ".$name."_BID");
	$wpdb->query("ALTER TABLE `wp_ulip_rate` DROP ".$name."_OFFER");
}
else{//selecting value to update	
	$columns = $wpdb->get_results($wpdb->prepare("show columns from wp_ulip_rate where Field !='id' and Field != 'Date'",$Field));
	foreach ($columns as $s ){
		$name=$s->Field;
		//echo $name;
	}
}
?>
<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/test/style-admin.css" rel="stylesheet" />
<div class="wrap">
<h2>Edit Product Ulip</h2>
<?php if($_POST['delete']){?>
<div class="updated"><p>deleted</p></div>
<a href="<?php echo admin_url('admin.php?page=ulip-list')?>">&laquo; Back to Product list</a>
<?php } else if($_POST['update']) {?>
<div class="updated"><p>Product updated</p></div>
<a href="<?php echo admin_url('admin.php?page=ulip-list')?>">&laquo; Back to Product list</a>
<?php } else {?>
<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
<table class='wp-list-table widefat fixed'>
<input type="hidden" name="old_name" value="<?php echo str_replace('_BID', '', $Field);?>" />
<tr><th>Name Product</th><td><input type="text" name="name" value="<?php echo str_replace('_BID', '', $Field);?>"/></td></tr>
</table>
<input type='submit' name="update" value='Edit' class='button'> &nbsp;&nbsp;
<input type='submit' name="delete" value='Delete' class='button' onclick="return confirm('Are you sure delete <?=str_replace('_BID', '', $Field)?> from product list?')">
</form>
<?php }?>

</div>
<?php
}

admin_ulip_update();