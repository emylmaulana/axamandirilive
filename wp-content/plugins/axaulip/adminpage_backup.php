<?php 

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class ulipListTable extends WP_List_Table {

function get_ulip_data(){
global $wpdb;

	$example_data = $wpdb->get_results('SELECT * FROM wp_ulip_rate ORDER BY Date ASC', ARRAY_A);
	return $example_data;
}

function __construct(){
global $status, $page;

    parent::__construct( array(
        'singular'  => __( 'uliprate', 'uliplisttable' ),     //singular name of the listed records
        'plural'    => __( 'uliprates', 'uliplisttable' ),   //plural name of the listed records
        'ajax'      => false        //does this table support ajax?
	) );
	
	}
	
function column_default( $item, $column_name ) {
	switch( $column_name ) { 
		case 'Date' :
			echo strftime('%e %h %Y',strtotime($item[$column_name]));
		break;
		case 'edit' :
			echo '<a href="admin.php?page=ulip-add&id='.$item['id'].'&action=edit">Edit</a>';
		break;
        case $column_name :
			//echo '<a href="admin.php?page=ulip-add&id='.$item['id'].'&action=edit">'.$item[$column_name].'</a>';	
			echo number_format($item[$column_name],2,',','.');
		break;
		
		}
	}

function get_columns(){
       global $wpdb;
		$column1 = $wpdb->get_results("show columns from wp_ulip_rate where Field!='id' "); //data columns
		$columns = array(); //array of wp_ulip_rate dari line 12
		foreach($column1 as $column){
			$field = $column->Field;
			$fields =$field;
			$lbl = str_replace('_', ' ', $column->Field);
			$label= "'".$lbl."', uliplistable";
			//var_dump($lbl);
			$columns[$column->Field] = __($lbl, 'uliplisttable');

		}
			$columns['edit'] = __( 'Edit', 'xratelisttable' );

         return $columns;
    } 

function get_sortable_columns(){
	$sortable_columns = array(
		'Date' => array('Date',true)
	);
	
	return $sortable_columns;
}
 
function prepare_items() {
	
	$per_page = 20;
	
	$columns  = $this->get_columns();
	$hidden   = array();
	$sortable = $this->get_sortable_columns();
	$this->_column_headers = array( $columns, $hidden, $sortable );
	
	$data = $this->get_ulip_data();

        function usort_reorder($a,$b){
            $orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'Date'; //If no sort, default to title
            $order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'asc'; //If no order, default to asc
            $result = strcmp($a[$orderby], $b[$orderby]); //Determine sort order
            return ($order==='asc') ? $result : -$result; //Send final sort direction to usort
        }
        usort($data, 'usort_reorder');
		
	$current_page = $this->get_pagenum();
    $total_items = count($data);
    $data = array_slice($data,(($current_page-1)*$per_page),$per_page);
    $this->items = $data;
	
        $this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items/$per_page)   //WE have to calculate the total number of pages
        ) );
	} 

}

function ulip_admin_page_add(){
	require_once('edit-ulip.php');
}

function ulip_render_page(){
  $myListTable = new ulipListTable();
  echo '</pre><div class="wrap"><h2>Today\'s ULIP Rate</h2>'; 
  $myListTable->prepare_items(); 
  $myListTable->display(); 
  echo '</div>'; 
}

?>